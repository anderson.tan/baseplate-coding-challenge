package com.appetiser.module.common.navigation

import android.content.Context
import android.content.Intent

/**
 * @author Anderson Tan on 12/18/2020.
 */

const val PREVIOUS_TIME_VISITED_SERVICE = "com.appetiser.baseplate.services.PreviousTimeVisitedService"

fun Context.previousTimeVisitedServiceIntent(): Intent =
    Intent().setClassName(packageName, PREVIOUS_TIME_VISITED_SERVICE)
