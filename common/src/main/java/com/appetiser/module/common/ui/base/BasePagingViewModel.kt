package com.appetiser.module.common.ui.base

import androidx.annotation.CallSuper
import androidx.paging.PagedList
import com.appetiser.module.common.InvokeError
import com.appetiser.module.common.InvokeStarted
import com.appetiser.module.common.InvokeStatus
import com.appetiser.module.common.InvokeSuccess
import com.appetiser.module.common.UiError
import com.appetiser.module.common.UiIdle
import com.appetiser.module.common.UiLoading
import com.appetiser.module.common.UiStatus
import com.appetiser.module.common.UiSuccess
import com.appetiser.module.common.paging.PagingViewState
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.BehaviorSubject

abstract class BasePagingViewModel<STATE : PagingViewState, LI>(
    initialState: STATE,
    private val pageSize: Int = 21 // depends on size of item visible on screen
) : BaseViewModel<STATE>(initialState) {

    protected val messages = BehaviorSubject.createDefault<UiStatus>(UiIdle)
    protected val loaded = BehaviorSubject.createDefault(false)

    abstract val pagedList: Observable<PagedList<LI>>

    protected open val pageListConfig = PagedList.Config.Builder().run {
        setPageSize(pageSize * 3)
        setPrefetchDistance(pageSize)
        setEnablePlaceholders(false)
        build()
    }

    protected val boundaryCallback = object : PagedList.BoundaryCallback<LI>() {
        override fun onItemAtEndLoaded(itemAtEnd: LI) = onListScrolledToEnd()

        override fun onItemAtFrontLoaded(itemAtFront: LI) {
            loaded.onNext(true)
        }

        override fun onZeroItemsLoaded() {
            loaded.onNext(true)
        }
    }

    fun onListScrolledToEnd() {
        callLoadMore()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnError { messages.onNext(UiError(it)) }
            .map {
                when (it) {
                    InvokeSuccess -> UiSuccess
                    InvokeStarted -> UiLoading(false)
                    is InvokeError -> UiError(it.throwable)
                    else -> UiIdle
                }
            }.subscribe { it -> messages.onNext(it) }
            .addTo(disposables)
    }

    fun refresh() = refresh(true)

    protected fun refresh(fromUser: Boolean, fullRefresh: Boolean = true) {
        callRefresh(fromUser)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnError { messages.onNext(UiError(it)) }
            .map {
                when (it) {
                    InvokeSuccess -> UiSuccess
                    InvokeStarted -> UiLoading(fullRefresh)
                    is InvokeError -> UiError(it.throwable)
                    else -> UiIdle
                }
            }.subscribe { messages.onNext(it) }
            .addTo(disposables)
    }

    @CallSuper
    protected open fun launchObserves() {
        messages
            .observeOn(schedulers.ui())
            .subscribeBy(onNext = ::setStatus)
            .addTo(disposables)

        loaded
            .observeOn(schedulers.ui())
            .subscribeBy(onNext = ::setLoaded)
            .addTo(disposables)
    }

    protected abstract fun callRefresh(fromUser: Boolean): Observable<InvokeStatus>

    protected abstract fun callLoadMore(): Observable<InvokeStatus>

    protected abstract fun setStatus(status: UiStatus)

    protected abstract fun setLoaded(isLoaded: Boolean)
}
