package com.appetiser.module.common.navigation

import android.content.Context
import android.content.Intent

/**
 * @author Anderson Tan on 12/18/2020.
 */

const val STORE_LIST_ACTIVITY = "com.appetiser.features.ui.storelists.StoreListActivity"
const val AUDIO_BOOK_DETAILS_ACTIVITY = "com.appetiser.features.ui.audiobook.AudioBookDetailsActivity"
const val SONG_DETAILS_ACTIVITY = "com.appetiser.features.ui.songdetails.SongDetailsActivity"
const val MOVIE_DETAILS_ACTIVITY = "com.appetiser.features.ui.moviedetails.MovieDetailsActivity"
const val TV_SHOW_DETAILS_ACTIVITY = "com.appetiser.features.ui.tvshowdetails.TvShowDetailsActivity"

const val TRACK_ID = "track_id"
const val COLLECTION_ID = "collection_id"

fun Context.songDetailsIntent(trackId: Long): Intent =
    Intent().setClassName(packageName, SONG_DETAILS_ACTIVITY)
        .putExtra(TRACK_ID, trackId)

fun Context.movieDetailsIntent(trackId: Long): Intent =
    Intent().setClassName(packageName, MOVIE_DETAILS_ACTIVITY)
        .putExtra(TRACK_ID, trackId)

fun Context.tvShowDetailsIntent(trackId: Long): Intent =
    Intent().setClassName(packageName, TV_SHOW_DETAILS_ACTIVITY)
        .putExtra(COLLECTION_ID, trackId)

fun Context.audioBookDetailsIntent(trackId: Long): Intent =
    Intent().setClassName(packageName, AUDIO_BOOK_DETAILS_ACTIVITY)
        .putExtra(COLLECTION_ID, trackId)
