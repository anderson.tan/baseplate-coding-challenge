package com.appetiser.module.common.ui

import com.appetiser.module.common.InvokeError
import com.appetiser.module.common.InvokeStarted
import com.appetiser.module.common.InvokeStatus
import com.appetiser.module.common.InvokeSuccess
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import java.util.concurrent.atomic.AtomicInteger

class ObservableLoadingCounter {
    private val count = AtomicInteger()
    private val loadingState = BehaviorSubject.createDefault(count.get())

    val observable: Observable<Boolean>
        get() = loadingState.map { it > 0 }.distinctUntilChanged()

    fun addLoader() {
        loadingState.onNext(count.incrementAndGet())
    }

    fun removeLoader() {
        loadingState.onNext(count.decrementAndGet())
    }
}

fun Observable<InvokeStatus>.collectInto(counter: ObservableLoadingCounter) = subscribe {
    when (it) {
        InvokeStarted -> counter.addLoader()
        InvokeSuccess, is InvokeError -> counter.removeLoader()
    }
}

fun Observable<InvokeStatus>.collectInto(
    counter: ObservableLoadingCounter,
    block: (InvokeStatus) -> Unit
) = subscribe {
    when (it) {
        InvokeStarted -> counter.addLoader()
        InvokeSuccess, is InvokeError -> counter.removeLoader()
    }
    block(it)
}
