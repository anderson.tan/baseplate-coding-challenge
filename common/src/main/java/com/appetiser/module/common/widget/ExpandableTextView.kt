package com.appetiser.module.common.widget

import android.content.Context
import android.text.Spannable
import android.text.SpannableString
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import com.appetiser.baseplate.common.R
import com.appetiser.module.common.afterMeasured

class ExpandableTextView : AppCompatTextView {
    private val readMoreText = " more"
    private val readMoreColor = ContextCompat.getColor(context, R.color.black_30)
    private var _maxLines = 2
    private var originalText: CharSequence? = null

    private var clickListener: OnClickMoreListener? = null

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        init(context)
    }

    private fun init(context: Context) {
        triggerTruncateText()
    }

    fun setOnMoreClickListener(listener: OnClickMoreListener) {
        this.clickListener = listener
    }

    override fun setText(text: CharSequence, type: BufferType) {
        super.setText(text, type)
        if (originalText == null) {
            originalText = text
        }
    }

    override fun getMaxLines(): Int {
        return _maxLines
    }

    override fun setMaxLines(maxLines: Int) {
        _maxLines = maxLines
    }

    fun triggerTruncateText() {
        afterMeasured {
            truncateText()
        }
    }

    private fun truncateText() {
        val maxLines = _maxLines
        val text = text.toString()
        if (lineCount >= maxLines) {
            val clickableSpan = object : ClickableSpan() {
                override fun onClick(widget: View) {
                    clickListener?.onClickMoreListener()
                }

                override fun updateDrawState(ds: TextPaint) {
                    ds.isUnderlineText = false
                }
            }

            val lineEndIndex = layout.getLineEnd(maxLines - 1)
            val truncatedText = getText().subSequence(0, lineEndIndex - readMoreText.length).toString() + readMoreText
            val spannable: Spannable = SpannableString(truncatedText)
            spannable.setSpan(ForegroundColorSpan(readMoreColor), truncatedText.length - readMoreText.length, truncatedText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            spannable.setSpan(
                clickableSpan,
                truncatedText.length - readMoreText.length,
                truncatedText.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            setText(spannable)
            super.setMaxLines(_maxLines)
        }
    }

    fun expandText() {
        text = originalText
        super.setMaxLines(1000)
    }

    fun reset() {
        originalText = null
    }

    interface OnClickMoreListener {
        fun onClickMoreListener()
    }
}
