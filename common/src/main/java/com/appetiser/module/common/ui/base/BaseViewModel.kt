package com.appetiser.module.common.ui.base

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.lifecycle.ViewModel
import com.appetiser.base.utils.schedulers.BaseSchedulerProvider
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

abstract class BaseViewModel<STATE>(
    private val initialState: STATE
) : ViewModel() {

    open val disposables: CompositeDisposable by lazy {
        CompositeDisposable()
    }

    private val _state = BehaviorSubject.createDefault(initialState)
    val state: Observable<STATE> = _state

    @Inject
    lateinit var schedulers: BaseSchedulerProvider

    /**
     * True when UI hasn't called ViewModel.onCreate yet.
     *
     * When activity/fragment is being reloaded view model will check this flag
     * so it doesn't need to reload data.
     */
    private var isFirstTimeUiCreate: Boolean = true

    /**
     * Called after fragment/activity view is created.
     */
    @CallSuper
    open fun onCreate(bundle: Bundle?) {
        if (isFirstTimeUiCreate) {
            isFirstTimeUiCreate(bundle)
            isFirstTimeUiCreate = false
        }
    }

    /**
     * Called when fragment is FIRST created. Changing screen orientation
     * does not trigger this method again because of ViewModel's lifecycle.
     *
     * Use this in loading data for your fragment.
     */
    abstract fun isFirstTimeUiCreate(bundle: Bundle?)

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

    protected fun setState(state: STATE.() -> STATE) {
        val newState = state(_state.value ?: initialState)
        _state.onNext(newState)
    }
}
