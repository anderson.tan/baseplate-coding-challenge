package com.appetiser.module.common.paging

import com.appetiser.module.common.UiStatus

interface PagingViewState {
    val status: UiStatus
    val isLoaded: Boolean
}
