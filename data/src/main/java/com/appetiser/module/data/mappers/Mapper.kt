package com.appetiser.module.data.mappers

interface Mapper<F, T> {
    operator fun invoke(from: F): T
}

interface IndexedMapper<F, T> {
    fun map(index: Int, from: F): T
}
