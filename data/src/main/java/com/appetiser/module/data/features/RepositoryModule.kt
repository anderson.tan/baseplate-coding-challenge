package com.appetiser.module.data.features

import com.appetiser.module.data.features.audiobookdetails.AudioBookRepository
import com.appetiser.module.data.features.audiobookdetails.AudioBookRepositoryImpl
import com.appetiser.module.data.features.moviedetails.MovieRepository
import com.appetiser.module.data.features.moviedetails.MovieRepositoryImpl
import com.appetiser.module.data.features.songdetails.SongRepository
import com.appetiser.module.data.features.songdetails.SongRepositoryImpl
import com.appetiser.module.data.features.storelist.StoreListRepository
import com.appetiser.module.data.features.storelist.StoreListRepositoryImpl
import com.appetiser.module.data.features.tvshowdetails.TvShowRepository
import com.appetiser.module.data.features.tvshowdetails.TvShowRepositoryImpl
import com.appetiser.module.data.mappers.AudioBookMapper
import com.appetiser.module.data.mappers.FeatureMovieMapper
import com.appetiser.module.data.mappers.SongMapper
import com.appetiser.module.data.mappers.StoreListMapper
import com.appetiser.module.data.mappers.TvEpisodeMapper
import com.appetiser.module.data.mappers.TvShowMapper
import com.appetiser.module.local.features.audiobookdetails.AudioBookLocalSource
import com.appetiser.module.local.features.moviedetails.MovieLocalSource
import com.appetiser.module.local.features.songdetails.SongLocalSource
import com.appetiser.module.local.features.storelist.StoreListLocalSource
import com.appetiser.module.local.features.tvshowdetails.TvShowLocalSource
import com.appetiser.module.network.features.storelist.StoreListRemoteSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun providesStoreListRepository(
        storeListRemoteSource: StoreListRemoteSource,
        storeListLocalSource: StoreListLocalSource,
        storeListMapper: StoreListMapper,
        songMapper: SongMapper,
        featureMovieMapper: FeatureMovieMapper,
        tvShowMapper: TvShowMapper,
        tvEpisodeMapper: TvEpisodeMapper,
        audioBookMapper: AudioBookMapper
    ): StoreListRepository {
        return StoreListRepositoryImpl(
            storeListRemoteSource,
            storeListLocalSource,
            storeListMapper,
            songMapper,
            featureMovieMapper,
            tvShowMapper,
            tvEpisodeMapper,
            audioBookMapper
        )
    }

    @Provides
    @Singleton
    fun providesAudioBookRepository(
        audioBookLocalSource: AudioBookLocalSource,
        audioBookMapper: AudioBookMapper
    ): AudioBookRepository {
        return AudioBookRepositoryImpl(
            audioBookLocalSource,
            audioBookMapper
        )
    }

    @Provides
    @Singleton
    fun providesMovieRepository(
        movieLocalSource: MovieLocalSource,
        movieMapper: FeatureMovieMapper
    ): MovieRepository {
        return MovieRepositoryImpl(
            movieLocalSource,
            movieMapper
        )
    }

    @Provides
    @Singleton
    fun providesSongRepository(
        songLocalSource: SongLocalSource,
        songMapper: SongMapper
    ): SongRepository {
        return SongRepositoryImpl(
            songLocalSource,
            songMapper
        )
    }

    @Provides
    @Singleton
    fun providesTvShowRepository(
        tvShowLocalSource: TvShowLocalSource,
        tvShowMapper: TvShowMapper,
        tvEpisodeMapper: TvEpisodeMapper
    ): TvShowRepository {
        return TvShowRepositoryImpl(
            tvShowLocalSource,
            tvShowMapper,
            tvEpisodeMapper
        )
    }
}
