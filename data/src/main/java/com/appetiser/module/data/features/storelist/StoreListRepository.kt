package com.appetiser.module.data.features.storelist

import androidx.paging.PagedList
import com.appetiser.module.domain.models.storelist.ItemDetails
import io.reactivex.Completable
import io.reactivex.Observable

interface StoreListRepository {
    fun observeItemPagedList(pagingConfig: PagedList.Config): Observable<PagedList<ItemDetails>>
    fun refreshItems(fromUser: Boolean): Completable
}
