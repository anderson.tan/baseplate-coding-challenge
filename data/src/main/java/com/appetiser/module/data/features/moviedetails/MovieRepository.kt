package com.appetiser.module.data.features.moviedetails

import com.appetiser.module.domain.models.moviedetails.FeatureMovie
import io.reactivex.Flowable

interface MovieRepository {
    fun observeMovie(trackId: Long): Flowable<FeatureMovie>

    fun observeRelatedMovies(
        collectionArtistId: Long
    ): Flowable<List<FeatureMovie>>

    fun observeUserMustLikeMovies(
        trackId: Long,
        collectionArtistId: Long
    ): Flowable<List<FeatureMovie>>
}
