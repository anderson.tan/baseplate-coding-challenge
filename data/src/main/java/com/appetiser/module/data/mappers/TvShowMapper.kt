package com.appetiser.module.data.mappers

import com.appetiser.module.domain.models.storelist.StoreItem
import com.appetiser.module.domain.models.tvshowdetails.TvEpisode
import com.appetiser.module.domain.models.tvshowdetails.TvShowDetails
import com.appetiser.module.local.entities.TvShowDB

class TvShowMapper : Mapper<StoreItem, TvShowDB> {

    fun fromDBToDomain(from: TvShowDB, tvEpisodes: List<TvEpisode> = emptyList()): TvShowDetails =
        TvShowDetails(
            id = from.trackId,
            artworkUrl100 = from.artworkUrl100,
            collectionName = from.collectionName,
            currency = from.currency,
            primaryGenreName = from.primaryGenreName,
            longDescription = from.longDescription,
            collectionPrice = from.collectionPrice,
            contentAdvisoryRating = from.contentAdvisoryRating,
            episodes = tvEpisodes
        )

    override operator fun invoke(from: StoreItem): TvShowDB =
        TvShowDB(
            id = from.collectionId,
            artworkUrl100 = from.artworkUrl100,
            collectionExplicitness = from.collectionExplicitness,
            collectionName = from.collectionName,
            collectionPrice = from.collectionPrice,
            collectionViewUrl = from.collectionViewUrl,
            contentAdvisoryRating = from.contentAdvisoryRating,
            currency = from.currency,
            discCount = from.discCount,
            discNumber = from.discNumber,
            primaryGenreName = from.primaryGenreName,
            longDescription = from.longDescription
        )
}
