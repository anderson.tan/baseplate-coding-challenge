package com.appetiser.module.data.mappers

import com.appetiser.module.domain.models.audiobookdetails.AudioBook
import com.appetiser.module.domain.models.storelist.StoreItem
import com.appetiser.module.local.entities.AudioBookDB

class AudioBookMapper : Mapper<StoreItem, AudioBookDB> {
    fun toDB(from: AudioBook): AudioBookDB =
        AudioBookDB(
            id = from.id,
            artistName = from.artistName,
            artworkUrl100 = from.artworkUrl100,
            collectionExplicitness = from.collectionExplicitness,
            collectionName = from.collectionName,
            collectionPrice = from.collectionPrice,
            collectionViewUrl = from.collectionViewUrl,
            currency = from.currency,
            previewUrl = from.previewUrl,
            primaryGenreName = from.primaryGenreName,
            releaseDate = from.releaseDate,
            trackCount = from.trackCount,
            wrapperType = from.wrapperType,
            artistViewUrl = from.artistViewUrl,
            description = from.description,
            artistId = from.artistId
        )

    fun fromDBToDomain(from: AudioBookDB): AudioBook =
        AudioBook(
            id = from.id,
            artistName = from.artistName,
            artworkUrl100 = from.artworkUrl100,
            collectionExplicitness = from.collectionExplicitness,
            collectionName = from.collectionName,
            collectionPrice = from.collectionPrice,
            collectionViewUrl = from.collectionViewUrl,
            currency = from.currency,
            previewUrl = from.previewUrl,
            primaryGenreName = from.primaryGenreName,
            releaseDate = from.releaseDate,
            trackCount = from.trackCount,
            wrapperType = from.wrapperType,
            artistViewUrl = from.artistViewUrl,
            description = from.description,
            artistId = from.artistId
        )

    override fun invoke(from: StoreItem): AudioBookDB =
        AudioBookDB(
            id = from.collectionId,
            artistName = from.artistName,
            artworkUrl100 = from.artworkUrl100,
            collectionExplicitness = from.collectionExplicitness,
            collectionName = from.collectionName,
            collectionPrice = from.collectionPrice,
            collectionViewUrl = from.collectionViewUrl,
            currency = from.currency,
            previewUrl = from.previewUrl,
            primaryGenreName = from.primaryGenreName,
            releaseDate = from.releaseDate,
            trackCount = from.trackCount,
            wrapperType = from.wrapperType,
            artistViewUrl = from.artistViewUrl,
            description = from.description,
            artistId = from.artistId
        )
}
