package com.appetiser.module.data.features.songdetails

import com.appetiser.module.data.mappers.SongMapper
import com.appetiser.module.domain.models.songdetails.Song
import com.appetiser.module.local.features.songdetails.SongLocalSource
import io.reactivex.Flowable

class SongRepositoryImpl(
    private val songLocalSource: SongLocalSource,
    private val songMapper: SongMapper
) : SongRepository {

    override fun observeSong(trackId: Long): Flowable<Song> {
        return songLocalSource.observeSong(trackId).map {
            songMapper.fromDBToDomain(it)
        }
    }

    override fun observeRelatedSongs(trackId: Long, artistId: Long): Flowable<List<Song>> {
        return songLocalSource.observeRelatedSongs(trackId, artistId).map {
            it.map { item -> songMapper.fromDBToDomain(item) }
        }
    }

    override fun observeSongsByAlbum(collectionId: Long): Flowable<List<Song>> {
        return songLocalSource.observeSongsByAlbum(collectionId).map {
            it.map { item -> songMapper.fromDBToDomain(item) }
        }
    }
}
