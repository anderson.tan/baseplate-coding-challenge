package com.appetiser.module.data.features.moviedetails

import com.appetiser.module.data.mappers.FeatureMovieMapper
import com.appetiser.module.domain.models.moviedetails.FeatureMovie
import com.appetiser.module.local.features.moviedetails.MovieLocalSource
import io.reactivex.Flowable

class MovieRepositoryImpl(
    private val movieLocalSource: MovieLocalSource,
    private val featureMovieMapper: FeatureMovieMapper
) : MovieRepository {

    override fun observeMovie(trackId: Long): Flowable<FeatureMovie> {
        return movieLocalSource.observeMovie(trackId).map {
            featureMovieMapper.fromDBToDomain(it)
        }
    }

    override fun observeRelatedMovies(collectionArtistId: Long): Flowable<List<FeatureMovie>> {
        return movieLocalSource.observeRelatedMovies(collectionArtistId).map {
            it.map { item -> featureMovieMapper.fromDBToDomain(item) }
        }
    }

    override fun observeUserMustLikeMovies(
        trackId: Long,
        collectionArtistId: Long
    ): Flowable<List<FeatureMovie>> {
        return movieLocalSource.observeUserMustLikeMovies(trackId, collectionArtistId).map {
            it.map { item -> featureMovieMapper.fromDBToDomain(item) }
        }
    }
}
