package com.appetiser.module.data.mappers

import com.appetiser.module.domain.models.storelist.ItemDetails
import com.appetiser.module.domain.models.storelist.ListItemType
import com.appetiser.module.local.features.storelist.models.ItemDetailsDB

class StoreListMapper : Mapper<ItemDetailsDB, ItemDetails> {

    override operator fun invoke(from: ItemDetailsDB): ItemDetails =
        with(from) {
            ItemDetails(
                id = itemEntry.itemId,
                imageUrl = imageUrl(),
                currency = currency(),
                genre = genre(),
                kind = kind(),
                trackName = trackName(),
                trackPrice = trackPrice()
            )
        }

    private fun ItemDetailsDB.trackName(): String = when (itemEntry.kind) {
        ListItemType.SONG -> song!!.trackName
        ListItemType.FEATURE_MOVIE -> featureMovie!!.trackName
        ListItemType.TV_SHOW -> tvShow!!.collectionName
        ListItemType.AUDIOBOOK -> audioBook!!.collectionName
        else -> "No track Name"
    }

    private fun ItemDetailsDB.genre(): String = when (itemEntry.kind) {
        ListItemType.SONG -> song!!.primaryGenreName
        ListItemType.FEATURE_MOVIE -> featureMovie!!.primaryGenreName
        ListItemType.TV_SHOW -> tvShow!!.primaryGenreName
        ListItemType.AUDIOBOOK -> audioBook!!.primaryGenreName
        else -> "No Genre"
    }

    private fun ItemDetailsDB.imageUrl(): String = when (itemEntry.kind) {
        ListItemType.SONG -> song!!.artworkUrl100
        ListItemType.FEATURE_MOVIE -> featureMovie!!.artworkUrl100
        ListItemType.TV_SHOW -> tvShow!!.artworkUrl100
        ListItemType.AUDIOBOOK -> audioBook!!.artworkUrl100
        else -> ""
    }

    private fun ItemDetailsDB.trackPrice(): Double = when (itemEntry.kind) {
        ListItemType.SONG -> song!!.trackPrice
        ListItemType.FEATURE_MOVIE -> featureMovie!!.trackPrice
        ListItemType.TV_SHOW -> tvShow!!.collectionPrice
        ListItemType.AUDIOBOOK -> audioBook!!.collectionPrice
        else -> 0.0
    }

    private fun ItemDetailsDB.currency(): String = when (itemEntry.kind) {
        ListItemType.SONG -> song!!.currency
        ListItemType.FEATURE_MOVIE -> featureMovie!!.currency
        ListItemType.TV_SHOW -> tvShow!!.currency
        ListItemType.AUDIOBOOK -> audioBook!!.currency
        else -> ""
    }

    private fun ItemDetailsDB.kind(): ListItemType = itemEntry.kind
}
