package com.appetiser.module.data.features.songdetails

import com.appetiser.module.domain.models.songdetails.Song
import io.reactivex.Flowable

interface SongRepository {
    fun observeSong(trackId: Long): Flowable<Song>
    fun observeRelatedSongs(trackId: Long, artistId: Long): Flowable<List<Song>>
    fun observeSongsByAlbum(collectionId: Long): Flowable<List<Song>>
}
