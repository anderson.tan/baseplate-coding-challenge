package com.appetiser.module.data.mappers

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MapperModule {

    @Provides
    @Singleton
    fun providesAudioBookMapper(): AudioBookMapper = AudioBookMapper()

    @Provides
    @Singleton
    fun providesFeatureMovieMapper(): FeatureMovieMapper = FeatureMovieMapper()

    @Provides
    @Singleton
    fun providesSongMapper(): SongMapper = SongMapper()

    @Provides
    @Singleton
    fun providesStoreListMapper(): StoreListMapper = StoreListMapper()

    @Provides
    @Singleton
    fun providesTvEpisodeMapper(): TvEpisodeMapper = TvEpisodeMapper()

    @Provides
    @Singleton
    fun providesTvShowMapper(): TvShowMapper = TvShowMapper()
}
