package com.appetiser.module.data.features.audiobookdetails

import com.appetiser.module.data.mappers.AudioBookMapper
import com.appetiser.module.domain.models.audiobookdetails.AudioBook
import com.appetiser.module.local.features.audiobookdetails.AudioBookLocalSource
import io.reactivex.Flowable

class AudioBookRepositoryImpl(
    private val audioBookLocalSource: AudioBookLocalSource,
    private val audioBookMapper: AudioBookMapper
) : AudioBookRepository {

    override fun observeAudioBook(collectionId: Long): Flowable<AudioBook> {
        return audioBookLocalSource.observeAudioBook(collectionId).map {
            audioBookMapper.fromDBToDomain(it)
        }
    }
}
