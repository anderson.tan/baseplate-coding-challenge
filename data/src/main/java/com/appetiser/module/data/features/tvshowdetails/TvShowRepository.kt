package com.appetiser.module.data.features.tvshowdetails

import com.appetiser.module.domain.models.tvshowdetails.TvShowDetails
import io.reactivex.Flowable

interface TvShowRepository {
    fun observeTvShowAndEpisodes(collectionId: Long): Flowable<TvShowDetails>
}
