package com.appetiser.module.data.mappers

import com.appetiser.module.domain.models.moviedetails.FeatureMovie
import com.appetiser.module.domain.models.storelist.StoreItem
import com.appetiser.module.local.entities.FeatureMovieDB

class FeatureMovieMapper : Mapper<StoreItem, FeatureMovieDB> {

    fun toDB(from: FeatureMovie): FeatureMovieDB =
        FeatureMovieDB(
            id = from.id,
            artistName = from.artistName,
            artworkUrl100 = from.artworkUrl100,
            collectionExplicitness = from.collectionExplicitness,
            collectionId = from.collectionId,
            collectionName = from.collectionName,
            collectionPrice = from.collectionPrice,
            collectionViewUrl = from.collectionViewUrl,
            currency = from.currency,
            discCount = from.discCount,
            discNumber = from.discNumber,
            previewUrl = from.previewUrl,
            primaryGenreName = from.primaryGenreName,
            releaseDate = from.releaseDate,
            trackCount = from.trackCount,
            trackExplicitness = from.trackExplicitness,
            trackName = from.trackName,
            trackNumber = from.trackNumber,
            trackPrice = from.trackPrice,
            trackTimeMillis = from.trackTimeMillis,
            trackViewUrl = from.trackViewUrl,
            wrapperType = from.wrapperType,
            collectionArtistId = from.collectionArtistId,
            contentAdvisoryRating = from.contentAdvisoryRating,
            longDescription = from.longDescription,
            trackHdPrice = from.trackHdPrice,
            trackHdRentalPrice = from.trackHdRentalPrice
        )

    fun fromDBToDomain(from: FeatureMovieDB): FeatureMovie =
        FeatureMovie(
            id = from.id,
            artistName = from.artistName,
            artworkUrl100 = from.artworkUrl100,
            collectionExplicitness = from.collectionExplicitness,
            collectionId = from.collectionId,
            collectionName = from.collectionName,
            collectionPrice = from.collectionPrice,
            collectionViewUrl = from.collectionViewUrl,
            currency = from.currency,
            discCount = from.discCount,
            discNumber = from.discNumber,
            previewUrl = from.previewUrl,
            primaryGenreName = from.primaryGenreName,
            releaseDate = from.releaseDate,
            trackCount = from.trackCount,
            trackExplicitness = from.trackExplicitness,
            trackName = from.trackName,
            trackNumber = from.trackNumber,
            trackPrice = from.trackPrice,
            trackTimeMillis = from.trackTimeMillis,
            trackViewUrl = from.trackViewUrl,
            wrapperType = from.wrapperType,
            collectionArtistId = from.collectionArtistId,
            contentAdvisoryRating = from.contentAdvisoryRating,
            longDescription = from.longDescription,
            trackHdPrice = from.trackHdPrice,
            trackHdRentalPrice = from.trackHdRentalPrice
        )

    override operator fun invoke(from: StoreItem): FeatureMovieDB =
        FeatureMovieDB(
            id = from.trackId,
            artistName = from.artistName,
            artworkUrl100 = from.artworkUrl100,
            collectionExplicitness = from.collectionExplicitness,
            collectionId = from.collectionId,
            collectionName = from.collectionName,
            collectionPrice = from.collectionPrice,
            collectionViewUrl = from.collectionViewUrl,
            currency = from.currency,
            discCount = from.discCount,
            discNumber = from.discNumber,
            previewUrl = from.previewUrl,
            primaryGenreName = from.primaryGenreName,
            releaseDate = from.releaseDate,
            trackCount = from.trackCount,
            trackExplicitness = from.trackExplicitness,
            trackName = from.trackName,
            trackNumber = from.trackNumber,
            trackPrice = from.trackPrice,
            trackTimeMillis = from.trackTimeMillis,
            trackViewUrl = from.trackViewUrl,
            wrapperType = from.wrapperType,
            collectionArtistId = from.collectionArtistId,
            contentAdvisoryRating = from.contentAdvisoryRating,
            longDescription = from.longDescription,
            trackHdPrice = from.trackHdPrice,
            trackHdRentalPrice = from.trackHdRentalPrice
        )
}
