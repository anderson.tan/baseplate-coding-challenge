package com.appetiser.module.data.features.tvshowdetails

import com.appetiser.module.data.mappers.TvEpisodeMapper
import com.appetiser.module.data.mappers.TvShowMapper
import com.appetiser.module.domain.models.tvshowdetails.TvShowDetails
import com.appetiser.module.local.features.tvshowdetails.TvShowLocalSource
import io.reactivex.Flowable
import javax.inject.Inject

class TvShowRepositoryImpl @Inject constructor(
    private val tvShowLocalSource: TvShowLocalSource,
    private val tvShowMapper: TvShowMapper,
    private val tvEpisodeMapper: TvEpisodeMapper
) : TvShowRepository {

    override fun observeTvShowAndEpisodes(collectionId: Long): Flowable<TvShowDetails> {
        return tvShowLocalSource.observeTvShowAndEpisodes(collectionId)
            .map {
                tvShowMapper.fromDBToDomain(
                    it.tvShow,
                    it.episodes.map { episode -> tvEpisodeMapper.fromDBToDomain(episode) })
            }
    }
}
