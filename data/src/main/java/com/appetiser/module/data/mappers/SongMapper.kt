package com.appetiser.module.data.mappers

import com.appetiser.module.domain.models.songdetails.Song
import com.appetiser.module.domain.models.storelist.StoreItem
import com.appetiser.module.local.entities.SongDB

class SongMapper : Mapper<StoreItem, SongDB> {

    fun fromDBToDomain(from: SongDB): Song =
        Song(
            id = from.trackId,
            artistId = from.artistId,
            artistName = from.artistName,
            artworkUrl100 = from.artworkUrl100,
            collectionId = from.collectionId,
            collectionName = from.collectionName,
            currency = from.currency,
            previewUrl = from.previewUrl,
            primaryGenreName = from.primaryGenreName,
            releaseDate = from.releaseDate,
            trackExplicitness = from.trackExplicitness,
            trackName = from.trackName,
            trackNumber = from.trackNumber,
            trackPrice = from.trackPrice,
            trackTimeMillis = from.trackTimeMillis
        )

    override operator fun invoke(from: StoreItem): SongDB =
        SongDB(
            id = from.trackId,
            artistId = from.artistId,
            artistName = from.artistName,
            artistViewUrl = from.artistViewUrl,
            artworkUrl100 = from.artworkUrl100,
            collectionExplicitness = from.collectionExplicitness,
            collectionId = from.collectionId,
            collectionName = from.collectionName,
            collectionPrice = from.collectionPrice,
            collectionViewUrl = from.collectionViewUrl,
            currency = from.currency,
            discCount = from.discCount,
            discNumber = from.discNumber,
            previewUrl = from.previewUrl,
            primaryGenreName = from.primaryGenreName,
            releaseDate = from.releaseDate,
            trackCount = from.trackCount,
            trackExplicitness = from.trackExplicitness,
            trackName = from.trackName,
            trackNumber = from.trackNumber,
            trackPrice = from.trackPrice,
            trackTimeMillis = from.trackTimeMillis,
            trackViewUrl = from.trackViewUrl,
            wrapperType = from.wrapperType
        )
}
