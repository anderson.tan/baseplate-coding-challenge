package com.appetiser.module.data.mappers

import com.appetiser.module.domain.models.storelist.StoreItem
import com.appetiser.module.domain.models.tvshowdetails.TvEpisode
import com.appetiser.module.local.entities.TvEpisodeDB

class TvEpisodeMapper : Mapper<StoreItem, TvEpisodeDB> {

    fun fromDBToDomain(from: TvEpisodeDB): TvEpisode =
        TvEpisode(
            id = from.trackId,
            artworkUrl100 = from.artworkUrl100,
            currency = from.currency,
            longDescription = from.longDescription,
            trackName = from.trackName,
            trackPrice = from.trackPrice,
            trackNumber = from.trackNumber,
            trackViewUrl = from.trackViewUrl
        )

    override operator fun invoke(from: StoreItem): TvEpisodeDB =
        TvEpisodeDB(
            id = from.trackId,
            artistName = from.artistName,
            artworkUrl100 = from.artworkUrl100,
            collectionExplicitness = from.collectionExplicitness,
            collectionId = from.collectionId,
            collectionName = from.collectionName,
            collectionPrice = from.collectionPrice,
            collectionViewUrl = from.collectionViewUrl,
            currency = from.currency,
            discCount = from.discCount,
            discNumber = from.discNumber,
            previewUrl = from.previewUrl,
            primaryGenreName = from.primaryGenreName,
            releaseDate = from.releaseDate,
            trackCount = from.trackCount,
            trackExplicitness = from.trackExplicitness,
            trackName = from.trackName,
            trackNumber = from.trackNumber,
            trackPrice = from.trackPrice,
            trackTimeMillis = from.trackTimeMillis,
            trackViewUrl = from.trackViewUrl,
            wrapperType = from.wrapperType,
            collectionArtistId = from.collectionArtistId,
            contentAdvisoryRating = from.contentAdvisoryRating,
            longDescription = from.longDescription,
            trackHdPrice = from.trackHdPrice,
            artistViewUrl = from.artistViewUrl
        )
}
