package com.appetiser.module.data.features.storelist

import androidx.paging.PagedList
import androidx.paging.RxPagedListBuilder
import com.appetiser.module.data.mappers.AudioBookMapper
import com.appetiser.module.data.mappers.FeatureMovieMapper
import com.appetiser.module.data.mappers.SongMapper
import com.appetiser.module.data.mappers.StoreListMapper
import com.appetiser.module.data.mappers.TvEpisodeMapper
import com.appetiser.module.data.mappers.TvShowMapper
import com.appetiser.module.domain.models.storelist.ItemDetails
import com.appetiser.module.domain.models.storelist.ListItemType
import com.appetiser.module.domain.models.storelist.responseToTrackType
import com.appetiser.module.local.entities.StoreItem
import com.appetiser.module.local.entities.TvShowDB
import com.appetiser.module.local.features.storelist.StoreListLocalSource
import com.appetiser.module.network.features.storelist.StoreListRemoteSource
import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject

class StoreListRepositoryImpl @Inject constructor(
    private val storeListRemoteSource: StoreListRemoteSource,
    private val storeListLocalSource: StoreListLocalSource,
    private val storeListMapper: StoreListMapper,
    private val songMapper: SongMapper,
    private val featureMovieMapper: FeatureMovieMapper,
    private val tvShowMapper: TvShowMapper,
    private val tvEpisodeMapper: TvEpisodeMapper,
    private val audioBookMapper: AudioBookMapper
) : StoreListRepository {

    override fun observeItemPagedList(pagingConfig: PagedList.Config): Observable<PagedList<ItemDetails>> {
        return storeListLocalSource.getPagedItems().flatMapObservable {
            RxPagedListBuilder(it.map { storeListDB ->
                storeListMapper(storeListDB)
            }, pagingConfig).buildObservable()
        }
    }

    override fun refreshItems(fromUser: Boolean): Completable =
        if (!fromUser) storeListLocalSource.hasItemEntry()
            .flatMapCompletable {
                if (!it) fetchItems()
                else Completable.complete()
            }
        else fetchItems()

    private fun fetchItems(): Completable {
        return storeListRemoteSource.getItems().flatMapCompletable { items ->
            val storeItems: ArrayList<StoreItem> = arrayListOf()
            items.forEach {
                var tvShow = TvShowDB()
                val toBeAdded = when (responseToTrackType(it.kind)) {
                    ListItemType.SONG -> songMapper(it)
                    ListItemType.FEATURE_MOVIE -> featureMovieMapper(it)
                    ListItemType.TV_SHOW -> {
                        tvShow = tvShowMapper(it)
                        tvEpisodeMapper(it)
                    }
                    else -> {
                        if (it.wrapperType == "audiobook") {
                            audioBookMapper(it)
                        } else StoreItem.empty()
                    }
                }
                if (storeItems.firstOrNull { item ->
                        item is TvShowDB &&
                            item.id == tvShow.id
                    } == null && tvShow.id != 0L) {
                    storeItems.add(tvShow)
                }
                storeItems.add(toBeAdded)
            }
            storeListLocalSource.insertItems(storeItems)
        }
    }
}
