package com.appetiser.module.data.features.audiobookdetails

import com.appetiser.module.domain.models.audiobookdetails.AudioBook
import io.reactivex.Flowable

interface AudioBookRepository {
    fun observeAudioBook(collectionId: Long): Flowable<AudioBook>
}
