package com.appetiser.baseplate

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.appetiser.module.common.navigation.STORE_LIST_ACTIVITY

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showLists()
    }

    private fun showLists() {
        Intent().setClassName(
            packageName,
            STORE_LIST_ACTIVITY
        ).also {
            startActivity(it)
            finish()
        }
    }
}
