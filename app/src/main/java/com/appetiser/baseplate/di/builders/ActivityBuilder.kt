package com.appetiser.baseplate.di.builders

import com.appetiser.baseplate.di.scopes.ActivityScope
import com.appetiser.features.ui.audiobook.AudioBookDetailsActivity
import com.appetiser.features.ui.moviedetails.MovieDetailsActivity
import com.appetiser.features.ui.songdetails.SongDetailsActivity
import com.appetiser.features.ui.storelists.StoreListActivity
import com.appetiser.features.ui.tvshowdetails.TvShowDetailsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeStoreListActivity(): StoreListActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeSongDetailsActivity(): SongDetailsActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeMovieDetailsActivity(): MovieDetailsActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeTvShowDetailsActivity(): TvShowDetailsActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeAudioBookDetailsActivity(): AudioBookDetailsActivity
}
