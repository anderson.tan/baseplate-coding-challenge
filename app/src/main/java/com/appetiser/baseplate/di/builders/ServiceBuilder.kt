package com.appetiser.baseplate.di.builders

import dagger.Module

@Module
abstract class ServiceBuilder
