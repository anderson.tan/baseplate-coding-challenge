package com.appetiser.baseplate.di

import android.app.Application
import com.appetiser.baseplate.BaseplateApplication
import com.appetiser.baseplate.di.builders.ActivityBuilder
import com.appetiser.baseplate.di.builders.FragmentBuilder
import com.appetiser.baseplate.di.builders.ServiceBuilder
import com.appetiser.module.data.features.RepositoryModule
import com.appetiser.module.data.mappers.MapperModule
import com.appetiser.module.local.StorageModule
import com.appetiser.module.local.features.DatabaseModule
import com.appetiser.module.network.NetworkModule
import com.appetiser.module.network.RemoteSourceModule
import com.appetiser.module.network.features.ApiServiceModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        StorageModule::class,
        FragmentBuilder::class,
        DatabaseModule::class,
        NetworkModule::class,
        ApiServiceModule::class,
        ActivityBuilder::class,
        SchedulerModule::class,
        ServiceBuilder::class,
        MapperModule::class,
        RepositoryModule::class,
        RemoteSourceModule::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: BaseplateApplication)
}
