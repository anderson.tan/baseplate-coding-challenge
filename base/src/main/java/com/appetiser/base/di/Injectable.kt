package com.appetiser.base.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
