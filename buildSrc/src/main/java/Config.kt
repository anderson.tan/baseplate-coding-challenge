const val kotlinVersion = "1.4.20"
const val androidPluginVersion = "4.0.1"
const val dexCountVersion = "0.8.2"
const val googlePlayVersion = "4.3.3"
const val crashlyticsGradlePluginVersion = "2.1.0"
const val navigationPluginVersion = "2.1.0-alpha04"

// Compile dependencies

// Support
const val appCompatVersion = "1.1.0-alpha03"
const val constraintLayoutVersion = "1.1.2"
const val supportMaterialDesignVersion = "1.2.0-beta01"
const val preferenceManagerVersion = "1.1.0"
const val coordinatorLayoutVersion = "1.1.0"
const val swipeRefreshLayoutVersion = "1.1.0"


const val lifecycleVersion = "2.2.0"

// Architecture Components
const val archCompVersion = "2.1.0"
const val archExtensionVersion = archCompVersion

private const val roomVersion = "2.2.5"

private const val coilVersion = "0.11.0"

// Sqlite encrpytioni
const val sqlChipherVersion = "4.3.0"
const val sqliteVersion = "2.0.1"

// Navigation Components
const val navigationVersion = "2.1.0-alpha04"

// Core-KTX
const val coreKtxVersion = "1.0.2"

// Google Maps
const val playServicesMapsVersion = "16.0.0"
const val playServicesLocationVersion = "16.0.0"
const val playServicesPlacesVersion = "16.0.0"
const val playServicesAuthVersion = "17.0.0"

// Timber
const val timberVersion = "4.7.1"

// Retrofit
const val retrofitVersion = "2.7.2"
const val retrofitScalarVersion = "2.7.2"

// OkHttp
const val okhttpVersion = "4.4.0"
const val okhttpLoggingVersion = okhttpVersion

// RxJava
const val rxJava2Version = "2.2.17"

// RxKotlin
const val rxKotlinVersion = "2.3.0"

// RxAndroid
const val rxAndroidVersion = "2.1.1"

// RxBinding
const val rxBindingXVersion = "3.0.0-alpha2"

// RxMath
const val rxMathVersion = "0.20.0"

// Dagger
const val daggerVersion = "2.20"

// MultiDex
const val multiDexVersion = "1.0.1"

// Glide
const val glideVersion = "4.10.0"

// GlassFish
const val glassFishVersion = "10.0-b28"

// Epoxy
const val epoxyVersion = "3.11.0"

// Paging
const val pagingVersion = "2.1.2"

// CircleImageView
const val circleImageViewVersion = "2.2.0"

// SnapHelper
const val snapHelperVersion = "1.5"

// RxPermissions
const val runtimePermissionVersion = "0.10.2"

// Stetho
const val stethoVersion = "1.5.0"

// Test dependency versions
const val junitVersion = "4.12"
const val testRunnerVersion = "1.1.0"
const val espressoVersion = "3.1.0"
const val mockitoVersion = "2.25.1"
const val gsonVersion = "2.8.5"

// Facebook SDK Version
const val facebookSdkVersion = "[5,6)"
const val facebookShimmerVersion = "0.5.0"

// ViewPager2
const val viewPager2Version = "1.0.0"

// Firebase Crashlytics
const val crashlyticsVersion = "17.0.0"
const val firebaseAnalyticsVersion = "17.4.2"
const val firebaseMessagingVersion = "20.2.0"

// Ucrop
const val uCropVersion = "2.2.5"

// libphonenumber
const val libphonenumberVersion = "8.12.7"

// Stripe
const val stripeVersion = "14.5.0"

// https://proandroiddev.com/gradle-dependency-management-with-kotlin-94eed4df9a28

object BuildPlugins {
    val androidPlugin = "com.android.tools.build:gradle:$androidPluginVersion"
    val kotlinPlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion"
    val crashlyticsGradlePlugin =
        "com.google.firebase:firebase-crashlytics-gradle:$crashlyticsGradlePluginVersion"
    val dexCountPlugin = "com.getkeepsafe.dexcount:dexcount-gradle-plugin:$dexCountVersion"
    val googlePlayPlugin = "com.google.gms:google-services:$googlePlayVersion"
    val navigationPlugin =
        "androidx.navigation:navigation-safe-args-gradle-plugin:$navigationPluginVersion"
}

object Android {
    // Manifest version information!
    private const val versionMajor = 0
    private const val versionMinor = 0
    private const val versionPatch = 0
    private const val versionBuild = 1 // bump for dogfood builds, public betas, etc.

    const val versionCode =
        versionMajor * 10000 + versionMinor * 1000 + versionPatch * 10 + versionBuild
    const val versionName = "$versionMajor.$versionMinor.$versionPatch"

    const val compileSdkVersion = 29
    const val targetSdkVersion = 29
    const val minSdkVersion = 26
    const val buildToolsVersion = "29.0.2"
}


@Suppress("UNUSED")
object Libs {

    // Kotlin
    const val kotlinStdlb = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlinVersion"

    // Support
    const val appCompat = "androidx.appcompat:appcompat:$appCompatVersion"
    const val cardView = "androidx.cardview:cardview:$appCompatVersion"
    const val recyclerView = "androidx.recyclerview:recyclerview:$appCompatVersion"
    const val constraintLayout =
        "androidx.constraintlayout:constraintlayout:$constraintLayoutVersion"
    const val supportDesign = "com.google.android.material:material:$supportMaterialDesignVersion"
    const val coordinatorLayout =
        "androidx.coordinatorlayout:coordinatorlayout:$coordinatorLayoutVersion"
    const val swipeRefreshLayout =
        "androidx.swiperefreshlayout:swiperefreshlayout:$swipeRefreshLayoutVersion"

    const val lifecycleExtensions = "androidx.lifecycle:lifecycle-extensions:$lifecycleVersion"
    const val lifecycleRuntime = "androidx.lifecycle:lifecycle-runtime-ktx:$lifecycleVersion"
    const val lifeCyleLiveData = "androidx.lifecycle:lifecycle-livedata-ktx:$lifecycleVersion"
    const val lifeCycleViewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycleVersion"

    // Architecture Components
    const val archExtensions = "androidx.lifecycle:lifecycle-extensions:$archExtensionVersion"
    const val archExtensionsCompiler = "androidx.lifecycle:lifecycle-compiler:$archExtensionVersion"
    const val room = "androidx.room:room-runtime:$roomVersion"
    const val roomRx = "androidx.room:room-rxjava2:$roomVersion"
    const val roomCompiler = "androidx.room:room-compiler:$roomVersion"

    // SQLCipher
    const val sqlcipher = "net.zetetic:android-database-sqlcipher:$sqlChipherVersion"
    const val sqlite = "androidx.sqlite:sqlite:$sqliteVersion"

    // Navigation Components
    const val navigationFragments = "androidx.navigation:navigation-fragment:$navigationVersion"
    const val navigationFragmentKtx =
        "androidx.navigation:navigation-fragment-ktx:$navigationVersion"
    const val navigationFragmentUI = "androidx.navigation:navigation-ui-ktx:$navigationVersion"

    // Core-KTX
    const val coreKTX = "androidx.core:core-ktx:$coreKtxVersion"

    // Google Maps
    const val playServicesMaps =
        "com.google.android.gms:play-services-maps:$playServicesMapsVersion"
    const val playServicesPlaces =
        "com.google.android.gms:play-services-places:$playServicesPlacesVersion"
    const val playServicesLocation =
        "com.google.android.gms:play-services-location:$playServicesLocationVersion"
    const val playServicesAuth =
        "com.google.android.gms:play-services-auth:$playServicesAuthVersion"

    // Timber
    const val timber = "com.jakewharton.timber:timber:$timberVersion"

    // Retrofit
    const val retrofit = "com.squareup.retrofit2:retrofit:$retrofitVersion"
    const val retrofitRxJava2 = "com.squareup.retrofit2:adapter-rxjava2:$retrofitVersion"
    const val retrofitGson = "com.squareup.retrofit2:converter-gson:$retrofitVersion"
    const val retrofitScalar = "com.squareup.retrofit2:converter-scalars:$retrofitScalarVersion"

    // OkHttp
    const val okhttp = "com.squareup.okhttp3:okhttp:$okhttpVersion"
    const val okhttpLogging = "com.squareup.okhttp3:logging-interceptor:$okhttpLoggingVersion"

    // RxJava
    const val rxjava2 = "io.reactivex.rxjava2:rxjava:$rxJava2Version"

    // RxKotlin
    const val rxkotlin = "io.reactivex.rxjava2:rxkotlin:$rxKotlinVersion"

    // RxAndroid
    const val rxandroid = "io.reactivex.rxjava2:rxandroid:$rxAndroidVersion"

    // RxBinding
    const val rxbindingAndroidX = "com.jakewharton.rxbinding3:rxbinding:$rxBindingXVersion"
    const val rxbindingAndroidXCore = "com.jakewharton.rxbinding3:rxbinding-core:$rxBindingXVersion"
    const val rxbindingAndroidXAppCompat =
        "com.jakewharton.rxbinding3:rxbinding-appcompat:$rxBindingXVersion"

    // Dagger
    const val dagger = "com.google.dagger:dagger:$daggerVersion"
    const val daggerCompiler = "com.google.dagger:dagger-compiler:$daggerVersion"
    const val daggerAndroid = "com.google.dagger:dagger-android:$daggerVersion"
    const val daggerSupport = "com.google.dagger:dagger-android-support:$daggerVersion"
    const val daggerProcessor = "com.google.dagger:dagger-android-processor:$daggerVersion"

    // MultiDex
    const val multiDex = "androidx.multidex:multidex:$multiDexVersion"

    // Glide
    const val glide = "com.github.bumptech.glide:glide:$glideVersion"
    const val glideCompiler = "com.github.bumptech.glide:compiler:$glideVersion"

    // GlassFish
    const val glassFishAnnotation = "org.glassfish:javax.annotation:$glassFishVersion"

    // Epoxy
    const val epoxy = "com.airbnb.android:epoxy:$epoxyVersion"
    const val epoxyPaging = "com.airbnb.android:epoxy-paging:$epoxyVersion"
    const val epoxyDataBinding = "com.airbnb.android:epoxy-databinding:$epoxyVersion"
    const val epoxyProcessor = "com.airbnb.android:epoxy-processor:$epoxyVersion"

    // Paging
    const val pagingCommon = "androidx.paging:paging-common-ktx:$pagingVersion"
    const val pagingRx = "androidx.paging:paging-rxjava2:$pagingVersion"

    // CircleImageView
    const val circleImageView = "de.hdodenhof:circleimageview:$circleImageViewVersion"

    // SnapHelper
    const val snapHelperPlugin = "com.github.rubensousa:gravitysnaphelper:$snapHelperVersion"

    // RxPermissions
    const val runtimePermission = "com.github.tbruyelle:rxpermissions:$runtimePermissionVersion"

    // Stetho
    const val stetho = "com.facebook.stetho:stetho:$stethoVersion"

    // Gson
    const val gson = "com.google.code.gson:gson:$gsonVersion"

    // PreferenceManager
    const val preference = "androidx.preference:preference-ktx:$preferenceManagerVersion"

    // Facebook SDK
    const val facebookSdk = "com.facebook.android:facebook-login:$facebookSdkVersion"
    const val facebookShimmer = "com.facebook.shimmer:shimmer:$facebookShimmerVersion"

    // ViewPager2
    const val viewPager2 = "androidx.viewpager2:viewpager2:$viewPager2Version"

    // firebase
    const val crashlytics = "com.google.firebase:firebase-crashlytics:$crashlyticsVersion"
    const val firebaseAnalytics =
        "com.google.firebase:firebase-analytics-ktx:$firebaseAnalyticsVersion"
    const val firebaseMessaging = "com.google.firebase:firebase-messaging:$firebaseMessagingVersion"

    // uCrop
    const val uCrop = "com.github.yalantis:ucrop:$uCropVersion"

    // libphonenumber
    const val libphonenumber = "com.googlecode.libphonenumber:libphonenumber:$libphonenumberVersion"

    // Stripe
    const val stripe = "com.stripe:stripe-android:$stripeVersion"

    const val coil = "io.coil-kt:coil:$coilVersion"
}

object TestLibs {
    const val junit = "junit:junit:$junitVersion"
    const val testRunner = "androidx.test:runner:$testRunnerVersion"
    const val espresso = "androidx.test.espresso:espresso-core:$espressoVersion"
    const val mockito = "org.mockito:mockito-inline:$mockitoVersion"
    const val archCoreTesting = "androidx.arch.core:core-testing:$archCompVersion"
}
