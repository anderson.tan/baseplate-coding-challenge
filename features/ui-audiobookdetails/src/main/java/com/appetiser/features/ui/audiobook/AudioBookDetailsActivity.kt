package com.appetiser.features.ui.audiobook

import android.content.Intent
import android.os.Bundle
import androidx.core.net.toUri
import com.appetiser.features.ui.audiobook.databinding.ActivityAudioBookDetailsBinding
import com.appetiser.module.common.ui.SpacingItemDecorator
import com.appetiser.module.common.ui.base.BaseViewModelActivity
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class AudioBookDetailsActivity :
    BaseViewModelActivity<ActivityAudioBookDetailsBinding, AudioBookDetailsViewState, AudioBookDetailsViewModel>() {

    private var controller: AudioBookDetailsEpoxyController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        controller = AudioBookDetailsEpoxyController()
        binding.toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24)
        binding.toolbar.setNavigationOnClickListener { onBackPressed() }
        binding.rvScrollable.apply {
            setController(controller!!.apply {
                callbacks = object : AudioBookDetailsEpoxyController.Callbacks {
                    override fun onPreviewUrl(url: String) {
                        startActivity(Intent(Intent.ACTION_VIEW).apply {
                            data = url.toUri()
                        })
                    }
                }
            })
            addItemDecoration(
                SpacingItemDecorator(
                    resources.getDimension(R.dimen.space_small).toInt()
                )
            )
        }

        viewModel.state
            .subscribeBy(onNext = ::render)
            .addTo(disposables)
    }

    override fun onDestroy() {
        binding.rvScrollable.clear()
        super.onDestroy()
        controller = null
    }

    private fun render(state: AudioBookDetailsViewState) {
        binding.state = state
        controller?.state = state
    }

    override fun getLayoutId(): Int = R.layout.activity_audio_book_details
}
