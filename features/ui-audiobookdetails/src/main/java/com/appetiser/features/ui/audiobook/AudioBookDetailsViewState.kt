package com.appetiser.features.ui.audiobook

import com.appetiser.module.domain.models.audiobookdetails.AudioBook

data class AudioBookDetailsViewState(
    val isLoading: Boolean = true,
    val audioBook: AudioBook = AudioBook()
)
