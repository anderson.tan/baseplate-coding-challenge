package com.appetiser.features.ui.audiobook

import android.os.Bundle
import com.appetiser.module.common.navigation.COLLECTION_ID
import com.appetiser.module.common.ui.ObservableLoadingCounter
import com.appetiser.module.common.ui.base.BaseViewModel
import com.appetiser.module.data.features.audiobookdetails.AudioBookRepository
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class AudioBookDetailsViewModel @Inject constructor(
    private val audioBookRepository: AudioBookRepository
) : BaseViewModel<AudioBookDetailsViewState>(
    AudioBookDetailsViewState()
) {
    private val audioBookLoading = ObservableLoadingCounter()

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        val collectionId = bundle?.takeIf { it.containsKey(COLLECTION_ID) }?.getLong(COLLECTION_ID)
        collectionId?.let {
            launchObserves(collectionId)
        }
    }

    private fun launchObserves(collectionId: Long) {
        audioBookRepository
            .observeAudioBook(collectionId)
            .observeOn(schedulers.ui())
            .doOnSubscribe { audioBookLoading.addLoader() }
            .subscribeBy {
                audioBookLoading.removeLoader()
                setState { copy(audioBook = it) }
            }.addTo(disposables)

        audioBookLoading
            .observable
            .observeOn(schedulers.ui())
            .subscribeBy { active ->
                setState { copy(isLoading = active) }
            }.addTo(disposables)
    }
}
