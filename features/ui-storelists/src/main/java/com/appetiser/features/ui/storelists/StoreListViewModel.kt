package com.appetiser.features.ui.storelists

import android.os.Bundle
import androidx.paging.PagedList
import com.appetiser.module.common.ui.base.BasePagingViewModel
import com.appetiser.module.common.InvokeError
import com.appetiser.module.common.InvokeStarted
import com.appetiser.module.common.InvokeStatus
import com.appetiser.module.common.InvokeSuccess
import com.appetiser.module.common.UiStatus
import com.appetiser.module.data.features.storelist.StoreListRepository
import com.appetiser.module.domain.models.storelist.ItemDetails
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject

class StoreListViewModel @Inject constructor(
    private val repository: StoreListRepository
) : BasePagingViewModel<StoreListViewState, ItemDetails>(
    StoreListViewState(),
    pageSize = 6
) {
    override val pagedList: Observable<PagedList<ItemDetails>> =
        repository.observeItemPagedList(pageListConfig)

    override fun callRefresh(fromUser: Boolean): Observable<InvokeStatus> {
        return Observable.create { emitter ->
            repository.refreshItems(fromUser)
                .doOnSubscribe { emitter.onNext(InvokeStarted) }
                .doOnComplete {
                    emitter.onNext(InvokeSuccess)
                }
                .subscribeBy(
                    onComplete = {
                        emitter.onNext(InvokeSuccess)
                    },
                    onError = {
                        Timber.d(it)
                        emitter.onNext(InvokeError(it))
                    }
                )
        }
    }

    override fun callLoadMore(): Observable<InvokeStatus> {
        return Observable.create { it.onNext(InvokeSuccess) }
    }

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        launchObserves()
        refresh(fromUser = false, fullRefresh = false)
    }

    override fun launchObserves() {
        super.launchObserves()

        pagedList
            .observeOn(schedulers.ui())
            .subscribeBy {
                setState { copy(isEmpty = it.size == 0) }
            }.addTo(disposables)
    }

    override fun setStatus(status: UiStatus) {
        setState { copy(status = status) }
    }

    override fun setLoaded(isLoaded: Boolean) {
        setState { copy(isLoaded = isLoaded) }
    }
}
