package com.appetiser.features.ui.storelists

import android.content.Intent
import android.os.Bundle
import androidx.annotation.LayoutRes
import com.appetiser.features.ui.storelists.databinding.ActivityStoreListBinding
import com.appetiser.module.common.UiError
import com.appetiser.module.common.UiLoading
import com.appetiser.module.common.navigation.audioBookDetailsIntent
import com.appetiser.module.common.navigation.movieDetailsIntent
import com.appetiser.module.common.navigation.previousTimeVisitedServiceIntent
import com.appetiser.module.common.navigation.songDetailsIntent
import com.appetiser.module.common.navigation.tvShowDetailsIntent
import com.appetiser.module.common.paging.PagingEpoxyController
import com.appetiser.module.common.ui.ProgressTimeLatch
import com.appetiser.module.common.ui.SpacingItemDecorator
import com.appetiser.module.common.ui.base.BaseViewModelActivity
import com.appetiser.module.domain.models.storelist.ItemDetails
import com.appetiser.module.domain.models.storelist.ListItemType
import com.google.android.material.snackbar.Snackbar
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class StoreListActivity :
    BaseViewModelActivity<ActivityStoreListBinding, StoreListViewState, StoreListViewModel>() {

    @LayoutRes
    override fun getLayoutId(): Int = R.layout.activity_store_list

    private lateinit var swipeRefreshLatch: ProgressTimeLatch
    private var controller: PagingEpoxyController<
        StoreListViewState,
        ItemDetails,
        ItemBindingModel_>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbarTitle("Store")
        startService(previousTimeVisitedServiceIntent())
        controller = StoreListEpoxyController(this).apply {
            callbacks = object : StoreListEpoxyController.Callbacks {
                override fun onItemClicked(type: ListItemType, itemId: Long) {
                    when (type) {
                        ListItemType.SONG -> songDetailsIntent(itemId)
                        ListItemType.FEATURE_MOVIE -> movieDetailsIntent(itemId)
                        ListItemType.TV_SHOW -> tvShowDetailsIntent(itemId)
                        ListItemType.AUDIOBOOK -> audioBookDetailsIntent(itemId)
                        else -> { Intent() }
                    }.also { startActivity(it) }
                }
            }
        }
        swipeRefreshLatch = ProgressTimeLatch {
            binding.refresher.isRefreshing = it
        }

        binding.rvItemLists.apply {
            setController(controller!!)
            addItemDecoration(
                SpacingItemDecorator(
                    resources.getDimension(R.dimen.padding_small).toInt()
                )
            )
        }

        binding.refresher.setOnRefreshListener(viewModel::refresh)

        viewModel.pagedList
            .observeOn(scheduler.ui())
            .subscribeBy { controller?.submitList(it) }
            .addTo(disposables)

        viewModel.state
            .observeOn(scheduler.ui())
            .subscribeBy(onNext = ::render)
            .addTo(disposables)
    }

    override fun onDestroy() {
        stopService(previousTimeVisitedServiceIntent())
        super.onDestroy()
        binding.rvItemLists.clear()
        controller = null
    }

    private fun render(state: StoreListViewState) {
        controller!!.state = state

        when (val status = state.status) {
            is UiError -> {
                swipeRefreshLatch.refreshing = false
                Snackbar.make(binding.root, status.message, Snackbar.LENGTH_SHORT).show()
            }
            is UiLoading -> swipeRefreshLatch.refreshing = status.fullRefresh
            else -> swipeRefreshLatch.refreshing = false
        }
    }
}
