package com.appetiser.features.ui.storelists

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.ModelCollector
import com.appetiser.baseplate.common.headline3
import com.appetiser.module.common.observable
import com.appetiser.module.common.paging.PagingEpoxyController
import com.appetiser.module.domain.models.storelist.ItemDetails
import com.appetiser.module.domain.models.storelist.ListItemType
import com.appetiser.module.local.util.getLastUserVisitedTime
import com.appetiser.module.local.util.getPref
import java.text.SimpleDateFormat
import java.util.Locale

class StoreListEpoxyController(private var context: Context) :
    PagingEpoxyController<StoreListViewState, ItemDetails, ItemBindingModel_>(
        StoreListViewState(),
        true
    ) {

    private val dateFormatter: SimpleDateFormat =
        SimpleDateFormat("MMM dd, yyyy hh:mm:ss", Locale.US)

    var callbacks: Callbacks? by observable(null, ::requestModelBuild)

    interface Callbacks {
        fun onItemClicked(type: ListItemType, itemId: Long)
    }

    override fun insertHeaderModels(modelCollector: ModelCollector) {
        with(modelCollector) {
            headline3 {
                id("headline")
                val lastTimeVisited = context.getPref().getLastUserVisitedTime()
                val text = String.format(
                    "Last time visited : %s",
                    if (lastTimeVisited != null) dateFormatter.format(
                        lastTimeVisited
                    ) else ""
                )
                text(text)
                onBind { _, view, _ ->
                    val layoutParams =
                        view.dataBinding.root.layoutParams as? StaggeredGridLayoutManager.LayoutParams
                    layoutParams?.isFullSpan = true
                }
            }
        }
    }

    override fun buildItemModel(item: ItemDetails): EpoxyModel<*> {
        return ItemBindingModel_()
            .id(item.id)
            .name(item.trackName)
            .imageUrl(item.imageUrl)
            .genre(item.genre)
            .price(item.trackPrice)
            .currency(item.currency)
            .kind(item.kind.type)
            .onClickListener(View.OnClickListener {
                callbacks?.onItemClicked(item.kind, item.id)
            })
    }

    override fun buildItemPlaceholder(index: Int): ItemBindingModel_ =
        ItemBindingModel_()
            .id("item_placeholder_$index")
}
