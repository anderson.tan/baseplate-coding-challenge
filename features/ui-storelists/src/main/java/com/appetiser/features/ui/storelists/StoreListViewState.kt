package com.appetiser.features.ui.storelists

import com.appetiser.module.common.UiIdle
import com.appetiser.module.common.UiStatus
import com.appetiser.module.common.paging.PagingViewState

data class StoreListViewState(
    val isEmpty: Boolean = false,
    override val status: UiStatus = UiIdle,
    override val isLoaded: Boolean = false
) : PagingViewState
