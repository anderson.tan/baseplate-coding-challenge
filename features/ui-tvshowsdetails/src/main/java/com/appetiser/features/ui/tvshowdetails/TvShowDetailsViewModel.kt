package com.appetiser.features.ui.tvshowdetails

import android.os.Bundle
import com.appetiser.module.common.navigation.COLLECTION_ID
import com.appetiser.module.common.ui.base.BaseViewModel
import com.appetiser.module.common.ui.ObservableLoadingCounter
import com.appetiser.module.data.features.tvshowdetails.TvShowRepository
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class TvShowDetailsViewModel @Inject constructor(
    private val tvShowRepository: TvShowRepository
) : BaseViewModel<TvShowDetailsViewState>(
    TvShowDetailsViewState()
) {
    private val tvShowLoading = ObservableLoadingCounter()

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        val collectionId = bundle?.takeIf { it.containsKey(COLLECTION_ID) }?.getLong(COLLECTION_ID)
        collectionId?.let {
            launchObserves(collectionId)
        }
    }

    private fun launchObserves(trackId: Long) {
        tvShowRepository
            .observeTvShowAndEpisodes(trackId)
            .observeOn(schedulers.ui())
            .doOnSubscribe { tvShowLoading.addLoader() }
            .subscribeBy {
                tvShowLoading.removeLoader()
                setState { copy(tvShowWithEpisodes = it) }
            }.addTo(disposables)

        tvShowLoading
            .observable
            .observeOn(schedulers.ui())
            .subscribeBy { active ->
                setState { copy(isLoading = active) }
            }.addTo(disposables)
    }
}
