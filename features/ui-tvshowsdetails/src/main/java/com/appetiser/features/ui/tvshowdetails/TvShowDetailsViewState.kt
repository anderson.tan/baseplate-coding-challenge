package com.appetiser.features.ui.tvshowdetails

import com.appetiser.module.domain.models.tvshowdetails.TvShowDetails

data class TvShowDetailsViewState(
    val isLoading: Boolean = true,
    val tvShowWithEpisodes: TvShowDetails = TvShowDetails()
)
