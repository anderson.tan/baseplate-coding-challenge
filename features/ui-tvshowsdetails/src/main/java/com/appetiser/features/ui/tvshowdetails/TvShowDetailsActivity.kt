package com.appetiser.features.ui.tvshowdetails

import android.content.Intent
import android.os.Bundle
import androidx.core.net.toUri
import com.appetiser.features.ui.tvshowdetails.databinding.ActivityTvShowDetailsBinding
import com.appetiser.module.common.ui.base.BaseViewModelActivity
import com.appetiser.module.common.ui.SpacingItemDecorator
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class TvShowDetailsActivity :
    BaseViewModelActivity<ActivityTvShowDetailsBinding, TvShowDetailsViewState, TvShowDetailsViewModel>() {

    private var controller: TvShowDetailsEpoxyController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        controller = TvShowDetailsEpoxyController()
        binding.toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24)
        binding.toolbar.setNavigationOnClickListener { onBackPressed() }
        binding.rvScrollable.apply {
            setController(controller!!.apply {
                callbacks = object : TvShowDetailsEpoxyController.Callbacks {
                    override fun onPreviewUrl(url: String) {
                        startActivity(Intent(Intent.ACTION_VIEW).apply {
                            data = url.toUri()
                        })
                    }
                }
            })
            addItemDecoration(
                SpacingItemDecorator(
                    resources.getDimension(R.dimen.space_small).toInt()
                )
            )
        }

        viewModel.state
            .subscribeBy(onNext = ::render)
            .addTo(disposables)
    }

    override fun onDestroy() {
        binding.rvScrollable.clear()
        super.onDestroy()
        controller = null
    }

    private fun render(state: TvShowDetailsViewState) {
        binding.state = state
        controller?.state = state
    }

    override fun getLayoutId(): Int = R.layout.activity_tv_show_details
}
