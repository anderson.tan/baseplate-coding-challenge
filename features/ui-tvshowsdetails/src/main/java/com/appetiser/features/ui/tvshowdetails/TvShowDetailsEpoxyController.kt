package com.appetiser.features.ui.tvshowdetails

import com.airbnb.epoxy.EpoxyController
import com.appetiser.baseplate.common.headline2
import com.appetiser.baseplate.common.infiniteLoading
import com.appetiser.baseplate.common.separatorThin
import com.appetiser.baseplate.common.vertSpacerSmall
import com.appetiser.module.common.observable

internal class TvShowDetailsEpoxyController : EpoxyController() {
    var state: TvShowDetailsViewState by observable(TvShowDetailsViewState(), ::requestModelBuild)
    var callbacks: Callbacks? by observable(null, ::requestModelBuild)

    interface Callbacks {
        fun onPreviewUrl(url: String)
    }

    override fun buildModels() {
        headline2 {
            id("tv_show_headline")
            text("About")
        }

        tvShowDescription {
            id("description")
            text(state.tvShowWithEpisodes.longDescription)
        }

        vertSpacerSmall {
            id("space_before_season_title")
        }

        tvShowEpisodeTitle {
            id("title_seasons")
        }

        state.tvShowWithEpisodes.episodes.forEachIndexed { _, it ->

            tvShowEpisodeItem {
                id("episode_item_${it.id}")
                counter(it.trackNumber)
                title(it.trackName)
                description(it.longDescription)
                currency(it.currency)
                price(it.trackPrice)
                imageUrl(it.artworkUrl100)
                onViewPreviewClick { _ ->
                    callbacks?.onPreviewUrl(it.trackViewUrl)
                }
            }

            separatorThin {
                id("separator_${it.id}")
            }
        }

        vertSpacerSmall {
            id("space_bottom")
        }

        if (state.isLoading) {
            infiniteLoading {
                id("infinite_loading_tv_show_details")
            }
            return
        }
    }
}
