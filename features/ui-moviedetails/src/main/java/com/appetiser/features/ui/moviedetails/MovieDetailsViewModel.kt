package com.appetiser.features.ui.moviedetails

import android.os.Bundle
import com.appetiser.module.common.navigation.TRACK_ID
import com.appetiser.module.common.ui.base.BaseViewModel
import com.appetiser.module.common.ui.ObservableLoadingCounter
import com.appetiser.module.data.features.moviedetails.MovieRepository
import com.appetiser.module.domain.models.moviedetails.FeatureMovie
import io.reactivex.Flowable
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class MovieDetailsViewModel @Inject constructor(
    private val movieRepository: MovieRepository
) : BaseViewModel<MovieDetailsViewState>(
    MovieDetailsViewState()
) {
    private val movieLoading = ObservableLoadingCounter()
    private val relatedMoviesLoading = ObservableLoadingCounter()
    private val userMustLikeMoviesLoading = ObservableLoadingCounter()

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        val trackId = bundle?.takeIf { it.containsKey(TRACK_ID) }?.getLong(TRACK_ID)
        trackId?.let {
            launchObserves(trackId)
        }
    }

    private fun launchObserves(trackId: Long) {
        movieRepository
            .observeMovie(trackId)
            .observeOn(schedulers.ui())
            .doOnSubscribe { movieLoading.addLoader() }
            .flatMap {
                movieLoading.removeLoader()
                setState { copy(movie = it) }
                Flowable.zip(
                    movieRepository.observeRelatedMovies(it.collectionArtistId),
                    movieRepository.observeUserMustLikeMovies(trackId, it.collectionArtistId),
                    BiFunction<List<FeatureMovie>, List<FeatureMovie>,
                        Pair<List<FeatureMovie>, List<FeatureMovie>>> { relatedMovies, userMustLikeMovies ->
                        relatedMovies to userMustLikeMovies
                    }
                )
            }
            .doOnSubscribe {
                relatedMoviesLoading.addLoader()
                userMustLikeMoviesLoading.addLoader()
            }
            .subscribeBy(onNext = {
                relatedMoviesLoading.removeLoader()
                userMustLikeMoviesLoading.removeLoader()
                setState {
                    copy(
                        relatedMovies = it.first,
                        userMustLikeMovies = it.second
                    )
                }
            }, onError = {
                relatedMoviesLoading.removeLoader()
                userMustLikeMoviesLoading.removeLoader()
            }).addTo(disposables)

        movieLoading
            .observable
            .observeOn(schedulers.ui())
            .subscribeBy { active ->
                setState { copy(isLoading = active) }
            }.addTo(disposables)

        relatedMoviesLoading.observable
            .observeOn(schedulers.ui())
            .subscribeBy { active ->
                setState { copy(isRelatedMoviesLoading = active) }
            }.addTo(disposables)

        userMustLikeMoviesLoading.observable
            .observeOn(schedulers.ui())
            .subscribeBy { active ->
                setState { copy(isUserMustLikeMoviesLoading = active) }
            }.addTo(disposables)
    }
}
