package com.appetiser.features.ui.moviedetails

import com.appetiser.module.domain.models.moviedetails.FeatureMovie

data class MovieDetailsViewState(
    val isLoading: Boolean = true,
    val isRelatedMoviesLoading: Boolean = true,
    val isUserMustLikeMoviesLoading: Boolean = true,
    val movie: FeatureMovie = FeatureMovie(),
    val relatedMovies: List<FeatureMovie> = emptyList(),
    val userMustLikeMovies: List<FeatureMovie> = emptyList()
)
