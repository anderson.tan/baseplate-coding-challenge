package com.appetiser.features.ui.songdetails

import android.content.Intent
import android.os.Bundle
import androidx.core.net.toUri
import com.appetiser.module.common.ui.base.BaseViewModelActivity
import com.appetiser.features.ui.songdetails.databinding.ActivitySongDetailsBinding
import com.appetiser.module.common.navigation.songDetailsIntent
import com.appetiser.module.common.ui.SpacingItemDecorator
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class SongDetailsActivity :
    BaseViewModelActivity<ActivitySongDetailsBinding, SongDetailsViewState, SongDetailsViewModel>() {

    private var controller: SongDetailsEpoxyController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        controller = SongDetailsEpoxyController()
        binding.toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24)
        binding.toolbar.setNavigationOnClickListener { onBackPressed() }
        binding.rvScrollable.apply {
            setController(controller!!.apply {
                callbacks = object : SongDetailsEpoxyController.Callbacks {
                    override fun onPreviewUrl(url: String) {
                        startActivity(Intent(Intent.ACTION_VIEW).apply {
                            data = url.toUri()
                        })
                    }

                    override fun onTrackClicked(trackId: Long) {
                        songDetailsIntent(trackId).also { startActivity(it) }
                    }
                }
            })
            addItemDecoration(
                SpacingItemDecorator(
                    resources.getDimension(R.dimen.space_small).toInt()
                )
            )
        }

        viewModel.state
            .subscribeBy(onNext = ::render)
            .addTo(disposables)
    }

    override fun onDestroy() {
        binding.rvScrollable.clear()
        super.onDestroy()
        controller = null
    }

    private fun render(state: SongDetailsViewState) {
        binding.state = state
        controller?.state = state
    }

    override fun getLayoutId(): Int = R.layout.activity_song_details
}
