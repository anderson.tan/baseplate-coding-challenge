package com.appetiser.features.ui.songdetails

import android.os.Bundle
import com.appetiser.module.common.ui.base.BaseViewModel
import com.appetiser.module.common.navigation.TRACK_ID
import com.appetiser.module.common.ui.ObservableLoadingCounter
import com.appetiser.module.data.features.songdetails.SongRepository
import com.appetiser.module.domain.models.songdetails.Song
import io.reactivex.Flowable
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class SongDetailsViewModel @Inject constructor(
    private val songRepository: SongRepository
) : BaseViewModel<SongDetailsViewState>(
    SongDetailsViewState()
) {
    private val songLoading = ObservableLoadingCounter()
    private val albumSongsLoading = ObservableLoadingCounter()
    private val relatedSongsLoading = ObservableLoadingCounter()

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        val trackId = bundle?.takeIf { it.containsKey(TRACK_ID) }?.getLong(TRACK_ID)
        trackId?.let {
            launchObserves(trackId)
        }
    }

    private fun launchObserves(trackId: Long) {
        songRepository
            .observeSong(trackId)
            .observeOn(schedulers.ui())
            .doOnSubscribe { songLoading.addLoader() }
            .flatMap {
                songLoading.removeLoader()
                setState { copy(song = it) }
                Flowable.zip(
                    songRepository.observeSongsByAlbum(it.collectionId),
                    songRepository.observeRelatedSongs(trackId, it.artistId),
                    BiFunction<List<Song>, List<Song>, Pair<List<Song>, List<Song>>> { albumSongs, relatedSongs ->
                        albumSongs to relatedSongs
                    }
                )
            }
            .doOnSubscribe {
                albumSongsLoading.addLoader()
                relatedSongsLoading.addLoader()
            }
            .subscribeBy(onNext = {
                relatedSongsLoading.removeLoader()
                albumSongsLoading.removeLoader()
                setState {
                    copy(
                        albumSongs = it.first,
                        relatedSongs = it.second
                    )
                }
            }, onError = {
                relatedSongsLoading.removeLoader()
                albumSongsLoading.removeLoader()
            }).addTo(disposables)

        songLoading
            .observable
            .observeOn(schedulers.ui())
            .subscribeBy { active ->
                setState { copy(isLoading = active) }
            }.addTo(disposables)

        relatedSongsLoading.observable
            .observeOn(schedulers.ui())
            .subscribeBy { active ->
                setState { copy(songsRelatedLoading = active) }
            }.addTo(disposables)

        albumSongsLoading.observable
            .observeOn(schedulers.ui())
            .subscribeBy { active ->
                setState { copy(albumSongsLoading = active) }
            }.addTo(disposables)
    }
}
