[![ktlint](https://img.shields.io/badge/code%20style-%E2%9D%A4-FF4081.svg)](https://ktlint.github.io/)

Language: Kotlin    
Architecture: MVVM    
Default Tools :  Dagger2, RxJava2, Architecture Components and etc.  see. app's build.gradle    
List of 3rd Party Libraries that are commonly used are found on "Config.kt"    

> Note : You can use any architecture layers as long as the MVVM and Dagger2 remains

  
**Code Style & Formatting**  
  
- Add Android Kotlin Style Guide [https://developer.android.com/kotlin/style-guide](https://developer.android.com/kotlin/style-guide)  
- Add KtLint for Kotlin linter and formatting [https://github.com/pinterest/ktlint](https://github.com/pinterest/ktlint)  

**Setup ktlint to your project**
- Copy the file ktlint.gradle and place it at the root of your project
- In your `app/build.gradle` file, add these lines in your android block `check.dependsOn ktlint` and `preBuild.dependsOn ktlint`
- To verify if the tasks are added, run `./gradlew tasks` and you should find the tasks `ktlint` and `ktlintFormat`
- Use `ktlint` to check the formatting
- Use `ktlintFormat` to fix the formatting
- When `ktintFormat` still fails, then you have a serious problem on your code not adhering to the guidelines
- Lastly, add a ktlint badge on your project because... why not? :)

```
android {
    compileSdkVersion 29
    ...

    check.dependsOn ktlint

    preBuild.dependsOn ktlint
}
```

  
## Project Structure
  
- [MVVM and](https://github.com/googlesamples/android-architecture) Repository pattern (optional), you can use any added layers as long as your base architecture is MVVM  
- [Dagger 2 Injector](https://medium.com/@iammert/new-android-injector-with-dagger-2-part-1-8baa60152abe)  
- [Architecture Components](https://developer.android.com/topic/libraries/architecture/)  [(LifeCycle,](https://developer.android.com/topic/libraries/architecture/) [Room, LiveData, ViewModel)](https://developer.android.com/topic/libraries/architecture/)
- [RxJava2](https://github.com/ReactiveX/RxJava)

#### Project Folder Structure
  
We are using package by feature [package by feature](https://medium.com/mindorks/pbf-package-by-feature-no-more-pbl-package-by-layer-50b8a9d54ae8)  , if you have something in mind, please try to reach us by creating an issue ticket and we will talk about it [[https://gitlab.com/appetiser/android-baseplate/issues](https://gitlab.com/appetiser/android-baseplate/issues)]
    
##### Class name
**([PascalCase](https://techterms.com/definition/pascalcase))**  
  
_Example:_  
- LoginActivity  
- LogoutActivity  
- RegisterFragment  
  
#### Package name
lower case concatenated words
  
  Example:
- myfirstapp  
- _myprofilepage_  
- _editprofile_ 
- _emailcheck_   
- _forgotpassword_   


#### Resources

**Naming.** Follow the convention of prefixing the type, as in `type_foo_bar.xml`. Examples: `fragment_contact_details.xml`, `view_primary_button.xml`, `activity_main.xml`.

**Organizing layout XMLs.** If you're unsure how to format a layout XML, the following convention may help.

- `android:id` name needs to be [(camelCase)](https://techterms.com/definition/camelcase) 
- `android:id` as the first attribute always,
- `style` at the top next to the `android:id`
- `android:layout_****` attributes at the top
- Tag closer `/>` on its own line, to facilitate ordering and adding attributes.
- Rather than hard coding `android:text`, consider using [Designtime attributes](http://tools.android.com/tips/layout-designtime-attributes) available for Android Studio.

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    >

    <TextView
        android:id="@+id/name"
        style="@style/FancyText"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_alignParentRight="true"
        android:text="@string/name"
        />

    <include layout="@layout/reusable_part" />

</LinearLayout>
```
**Use styles.** Almost every project needs to properly use styles, because it is very common to have a repeated appearance for a view. At least you should have a common style for most text content in the application, for example:

```xml
<style name="ContentText">
    <item name="android:textSize">@dimen/font_normal</item>
    <item name="android:textColor">@color/basic_black</item>
</style>
```

Applied to TextViews:

```xml
<TextView
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:text="@string/price"
    style="@style/ContentText"
    />
```

**`colors.xml` is a color palette.** There should be nothing in your `colors.xml` other than a mapping from a color name to an RGBA value. This helps avoid repeating RGBA values and as such will make it easy to change or refactor colors, and also will make it explicit how many different colors are being used. Normally for a aesthetic UI, it is important to reduce the variety of colors being used.
 
*So, don't define your colors.xml like this:*

```xml
<resources>
    <color name="button_foreground">#FFFFFF</color>
    <color name="button_background">#2A91BD</color>
</resources>    
```

Instead, do this:

```xml
<resources>
    <!-- grayscale -->
    <color name="white">#FFFFFF</color>
   
    <!-- basic colors -->
    <color name="blue">#2A91BD</color>
</resources>
```

Ask the designer of the application for this palette. The names do not need to be plain color names as "green", "blue", etc. Names such as "brand_primary", "brand_secondary", "brand_negative" are totally acceptable as well.

By referencing the color palette from your styles allows you to abstract the underlying colors from their usage in the app, as per:

- `colors.xml` - defines only the color palette.
- `styles.xml` - defines styles which reference the color palette and reflects the color usage. (e.g. the button foreground is white).
- `activity_main.xml` - references the appropriate style in `styles.xml` to color the button.

If needed, even further separation between underlying colors and style usage can be achieved by defined an additional color resource file which references the color palette. As per:

```xml
<color name="button_foreground">@color/white</color> 
<color name="button_background">@color/blue</color> 
```

Then in styles.xml:

```xml
<style name="AcceptButton">
    <item name="android:foreground">@color/button_foreground</item>
    <item name="android:background">@color/button_background</item>
</style>
```

This approach offers improved color refactoring and more stable style definitions when multiple related styles share similar color and usage properties. However, it comes at the cost of maintaining another set of color mappings. 

**Treat dimens.xml like colors.xml.** You should also define a "palette" of typical spacing and font sizes, for basically the same purposes as for colors. A good example of a dimens file:

```xml
<resources>

    <!-- font sizes -->
    <dimen name="font_larger">22sp</dimen>
    <dimen name="font_large">18sp</dimen>
    <dimen name="font_normal">15sp</dimen>
    <dimen name="font_small">12sp</dimen>

    <!-- typical spacing between two views -->
    <dimen name="spacing_huge">40dp</dimen>
    <dimen name="spacing_large">24dp</dimen>
    <dimen name="spacing_normal">14dp</dimen>
    <dimen name="spacing_small">10dp</dimen>
    <dimen name="spacing_tiny">4dp</dimen>

    <!-- typical sizes of views -->
    <dimen name="button_height_tall">60dp</dimen>
    <dimen name="button_height_normal">40dp</dimen>
    <dimen name="button_height_short">32dp</dimen>

</resources>
```

You should use the `spacing_****` dimensions for layouting, in margins and paddings, instead of hard-coded values, much like strings are normally treated. This will give a consistent look-and-feel, while making it easier to organize and change styles and layouts.

**strings.xml**

Name your strings with keys that resemble namespaces, and don't be afraid of repeating a value for two or more keys. Languages are complex, so namespaces are necessary to bring context and break ambiguity.

**Bad**
```xml
<string name="network_error">Network error</string>
<string name="call_failed">Call failed</string>
<string name="map_failed">Map loading failed</string>
```

**Good**
```xml
<string name="error_message_network">Network error</string>
<string name="error_message_call">Call failed</string>
<string name="error_message_map">Map loading failed</string>
```

Don't write string values in all uppercase. Stick to normal text conventions (e.g., capitalize first character). If you need to display the string in all caps, then do that using for instance the attribute [`textAllCaps`](http://developer.android.com/reference/android/widget/TextView.html#attr_android:textAllCaps) on a TextView.

**Bad**
```xml
<string name="error_message_call">CALL FAILED</string>
```

**Good**
```xml
<string name="error_message_call">Call failed</string>
```
  
  
  
## Project Configuration
**Build Variants**
  
- internalDebug
- productionRelease

  
  
## FAQ

**Can we remove ktlint?**
- No, ktlint helps us a lot, ktlint will do the code formatting for you. 
- Ktlint will only get triggered if you try to commit your code using `git commit`. It will auto format your code based on ktlint standards. If you will get an error, you need to fix it or else you can't `push` your code. :)
- There's a scenario that someone will contribute to your project and you don't have the same coding style, if they push their code to your project, there is a possibility that they can push unnecessary line of code (too much break line, white spaces, etc)

**Can we change the MVVM architecture?**
- No, because most of our apps are using MVVM and if you encountered some issues about your project, we can easily help you.

**Can we add new layer/s in our MVVM architecture?**
- Yes, as long as the core architecture is MVVM and repository pattern.

**Can we remove auth_modern or auth_traditional module?**
- Yes, it's up to you, if you do that, you need to transfer all the resources to your app module

**Can we remove dagger?**
- For now, No, Dagger helps us a lot. If you're not familiar with dagger, visit `https://appetiser.com.au/blog/dagger-a-new-dependency-injection-for-java-and-android/`

**Where can I add my gradle dependencies?**
- buildSrc/src/main/java/Config.kt

**Why `secret.gradle` is not ignored?**
- You can ignore `secret.gradle` if you want, it is optional

**Can I change the build variants?**
- Feel free


## Backend API
[baseplate backend](https://documenter.getpostman.com/view/8615821/SWTAAeEL?version=latest)
