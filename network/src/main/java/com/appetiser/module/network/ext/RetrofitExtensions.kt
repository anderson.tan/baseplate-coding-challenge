@file:Suppress("NOTHING_TO_INLINE")

package com.appetiser.module.network.ext

import com.appetiser.module.domain.core.ErrorResult
import com.appetiser.module.domain.core.Result
import com.appetiser.module.domain.core.Success
import okhttp3.ResponseBody
import retrofit2.HttpException
import retrofit2.Response

inline fun <T> Response<T>.bodyOrThrow(): T {
    if (!isSuccessful) throw HttpException(this)
    return body()!!
}

inline fun <T> Response<T>.toException() = HttpException(this)

inline fun <T> Response<T>.toResultUnit(): Result<Unit> = try {
    if (isSuccessful) {
        Success(data = Unit)
    } else {
        ErrorResult(toException())
    }
} catch (e: Exception) {
    ErrorResult(e)
}

inline fun <T> Response<T>.toResult(): Result<T> = try {
    if (isSuccessful) {
        Success(data = bodyOrThrow())
    } else {
        ErrorResult(toException())
    }
} catch (e: Exception) {
    ErrorResult(e)
}

fun <T, E> Response<T>.toResult(mapper: (T) -> E): Result<E> = try {
    if (isSuccessful) {
        Success(data = mapper(bodyOrThrow()))
    } else {
        ErrorResult(toException())
    }
} catch (e: Exception) {
    ErrorResult(e)
}

fun <T, E> Response<T>.toResult(
    mapper: (T) -> E,
    errorMapper: (ResponseBody) -> Throwable
): Result<E> = try {
    if (isSuccessful) {
        Success(data = mapper(bodyOrThrow()))
    } else {
        ErrorResult(errorMapper(errorBody()!!))
    }
} catch (e: Exception) {
    ErrorResult(e)
}
