package com.appetiser.module.network.features.storelist.models

import com.appetiser.module.domain.models.storelist.StoreItem

data class StoreItemDTO(
    val artistId: Long = 0,
    val artistName: String = "",
    val artistViewUrl: String = "",
    val artworkUrl100: String = "",
    val collectionArtistId: Long = 0,
    val collectionExplicitness: String = "",
    val collectionId: Long = 0,
    val collectionName: String = "",
    val collectionPrice: Double = 0.0,
    val collectionViewUrl: String = "",
    val contentAdvisoryRating: String = "",
    val currency: String = "",
    val description: String = "",
    val discCount: Int = 0,
    val discNumber: Int = 0,
    val kind: String = "",
    val longDescription: String = "",
    val previewUrl: String = "",
    val primaryGenreName: String = "",
    val releaseDate: String = "",
    val trackCount: Int = 0,
    val trackExplicitness: String = "",
    val trackHdPrice: Double = 0.0,
    val trackHdRentalPrice: Double = 0.0,
    val trackId: Long = 0,
    val trackName: String = "",
    val trackNumber: Int = 0,
    val trackPrice: Double = 0.0,
    val trackTimeMillis: Long = 0,
    val trackViewUrl: String = "",
    val wrapperType: String = ""
) {

    companion object {
        fun toDomain(dto: StoreItemDTO): StoreItem =
            with(dto) {
                StoreItem(
                    artistId = artistId,
                    artistName = artistName,
                    artistViewUrl = artistViewUrl,
                    artworkUrl100 = artworkUrl100,
                    collectionArtistId = collectionArtistId,
                    collectionExplicitness = collectionExplicitness,
                    collectionId = collectionId,
                    collectionName = collectionName,
                    collectionPrice = collectionPrice,
                    collectionViewUrl = collectionViewUrl,
                    contentAdvisoryRating = contentAdvisoryRating,
                    currency = currency,
                    description = description,
                    discCount = discCount,
                    discNumber = discNumber,
                    kind = kind,
                    longDescription = longDescription,
                    previewUrl = previewUrl,
                    primaryGenreName = primaryGenreName,
                    releaseDate = releaseDate,
                    trackCount = trackCount,
                    trackExplicitness = trackExplicitness,
                    trackHdPrice = trackHdPrice,
                    trackHdRentalPrice = trackHdRentalPrice,
                    trackId = trackId,
                    trackName = trackName,
                    trackNumber = trackNumber,
                    trackPrice = trackPrice,
                    trackTimeMillis = trackTimeMillis,
                    trackViewUrl = trackViewUrl,
                    wrapperType = wrapperType
                )
            }
    }
}
