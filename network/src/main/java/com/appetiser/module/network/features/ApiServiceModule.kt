package com.appetiser.module.network.features

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class ApiServiceModule {

    @Provides
    @Singleton
    fun providesStoreServices(retrofit: Retrofit): StoreService =
        retrofit.create(StoreService::class.java)
}
