package com.appetiser.module.network.features.storelist

import com.appetiser.module.domain.models.storelist.StoreItem
import com.appetiser.module.network.base.BaseRemoteSource
import com.appetiser.module.network.ext.toResult
import com.appetiser.module.network.features.StoreService
import com.appetiser.module.network.features.storelist.models.StoreItemDTO
import io.reactivex.Single
import javax.inject.Inject

class StoreListRemoteSourceImpl @Inject constructor(
    private val service: StoreService
) : BaseRemoteSource(), StoreListRemoteSource {

    override fun getItems(): Single<List<StoreItem>> {
        return service.getItems().map {
            it.toResult { response -> response.results!! }
        }.flatMapIterable { it.getOrThrow() }.map { StoreItemDTO.toDomain(it) }
            .toList()
    }
}
