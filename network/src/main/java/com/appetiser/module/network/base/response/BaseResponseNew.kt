package com.appetiser.module.network.base.response

import com.google.gson.annotations.SerializedName

data class BaseResponseNew<T>(
    val data: T,
    val success: Boolean = false,
    val message: String = "",
    @SerializedName("http_status") val httpStatus: Int = 500
)
