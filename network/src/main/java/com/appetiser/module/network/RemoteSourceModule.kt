package com.appetiser.module.network

import com.appetiser.module.network.features.StoreService
import com.appetiser.module.network.features.storelist.StoreListRemoteSource
import com.appetiser.module.network.features.storelist.StoreListRemoteSourceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RemoteSourceModule {

    @Provides
    @Singleton
    fun providesStoreListRemoteSource(
        storeService: StoreService
    ): StoreListRemoteSource = StoreListRemoteSourceImpl(storeService)
}
