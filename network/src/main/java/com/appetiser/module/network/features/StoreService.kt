package com.appetiser.module.network.features

import com.appetiser.module.network.base.response.StoreBaseResponse
import com.appetiser.module.network.features.storelist.models.StoreItemDTO
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET

interface StoreService {

    @GET("search?term=star&amp;country=au&amp;media=movie&amp;all")
    fun getItems(): Observable<Response<StoreBaseResponse<List<StoreItemDTO>>>>
}
