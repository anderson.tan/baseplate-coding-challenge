package com.appetiser.module.network.base.response

data class StoreBaseResponse<T>(
    val resultCount: Int = 0,
    val results: T? = null
)
