package com.appetiser.module.network.features.storelist

import com.appetiser.module.domain.models.storelist.StoreItem
import io.reactivex.Single

interface StoreListRemoteSource {
    fun getItems(): Single<List<StoreItem>>
}
