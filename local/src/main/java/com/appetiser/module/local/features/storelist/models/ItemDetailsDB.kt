package com.appetiser.module.local.features.storelist.models

import androidx.room.Embedded
import androidx.room.Relation
import com.appetiser.module.local.entities.AudioBookDB
import com.appetiser.module.local.entities.FeatureMovieDB
import com.appetiser.module.local.entities.ItemEntryDB
import com.appetiser.module.local.entities.SongDB
import com.appetiser.module.local.entities.TvShowDB

data class ItemDetailsDB(
    @Embedded
    var itemEntry: ItemEntryDB = ItemEntryDB(),

    @Relation(
        parentColumn = "item_id",
        entityColumn = "track_id"
    )
    var song: SongDB? = SongDB(),

    @Relation(
        parentColumn = "item_id",
        entityColumn = "track_id"
    )
    var featureMovie: FeatureMovieDB? = null,

    @Relation(
        parentColumn = "item_id",
        entityColumn = "collection_id"
    )
    var tvShow: TvShowDB? = null,

    @Relation(
        parentColumn = "item_id",
        entityColumn = "collection_id"
    )
    var audioBook: AudioBookDB? = null
)
