package com.appetiser.module.local.features.tvshowdetails.models

import androidx.room.Embedded
import androidx.room.Relation
import com.appetiser.module.local.entities.TvEpisodeDB
import com.appetiser.module.local.entities.TvShowDB

data class TvShowWithEpisodesDB(
    @Embedded
    var tvShow: TvShowDB = TvShowDB(),

    @Relation(
        parentColumn = "collection_id",
        entityColumn = "collection_id"
    )
    var episodes: List<TvEpisodeDB> = emptyList()
)
