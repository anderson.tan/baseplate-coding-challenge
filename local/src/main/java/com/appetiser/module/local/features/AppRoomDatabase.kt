package com.appetiser.module.local.features

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.appetiser.module.local.BuildConfig
import com.appetiser.module.local.entities.AudioBookDB
import com.appetiser.module.local.entities.FeatureMovieDB
import com.appetiser.module.local.entities.ItemEntryDB
import com.appetiser.module.local.entities.SongDB
import com.appetiser.module.local.entities.TvEpisodeDB
import com.appetiser.module.local.entities.TvShowDB
import com.appetiser.module.local.features.storelist.models.ItemConverter
import net.sqlcipher.database.SQLiteDatabase
import net.sqlcipher.database.SupportFactory

@Database(
    entities = [
        AudioBookDB::class,
        SongDB::class,
        FeatureMovieDB::class,
        TvShowDB::class,
        TvEpisodeDB::class,
        ItemEntryDB::class
    ],
    version = 1,
    exportSchema = true
)
@TypeConverters(ItemConverter::class)
abstract class AppRoomDatabase : RoomDatabase(), AppDatabase {

    companion object {
        @Volatile
        private var INSTANCE: AppRoomDatabase? = null

        fun getInstance(context: Context): AppRoomDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context): AppRoomDatabase {
            val dbName = "baseplate.db"

            val builder = Room.databaseBuilder(
                context,
                AppRoomDatabase::class.java, dbName
            ).fallbackToDestructiveMigration()

            if (!BuildConfig.DEBUG) {
                val passphrase = SQLiteDatabase.getBytes(dbName.toCharArray())
                val factory = SupportFactory(passphrase)
                builder.openHelperFactory(factory)
            }

            return builder.build()
        }
    }
}
