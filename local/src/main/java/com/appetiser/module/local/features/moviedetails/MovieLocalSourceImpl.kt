package com.appetiser.module.local.features.moviedetails

import com.appetiser.module.local.entities.FeatureMovieDB
import com.appetiser.module.local.features.moviedetails.dao.MovieDao
import io.reactivex.Flowable
import javax.inject.Inject

class MovieLocalSourceImpl @Inject constructor(
    private val movieDao: MovieDao
) : MovieLocalSource {
    override fun observeMovie(trackId: Long): Flowable<FeatureMovieDB> =
        movieDao.getMovie(trackId)

    override fun observeRelatedMovies(collectionArtistId: Long): Flowable<List<FeatureMovieDB>> =
        movieDao.getRelatedMovies(collectionArtistId)

    override fun observeUserMustLikeMovies(
        trackId: Long,
        collectionArtistId: Long
    ): Flowable<List<FeatureMovieDB>> = movieDao.getUserMustLikeMovies(trackId, collectionArtistId)
}
