package com.appetiser.module.local.features.tvshowdetails

import com.appetiser.module.local.features.tvshowdetails.dao.TvShowDao
import com.appetiser.module.local.features.tvshowdetails.models.TvShowWithEpisodesDB
import io.reactivex.Flowable
import javax.inject.Inject

class TvShowLocalSourceImpl @Inject constructor(
    private val tvShowDao: TvShowDao
) : TvShowLocalSource {

    override fun observeTvShowAndEpisodes(collectionId: Long): Flowable<TvShowWithEpisodesDB> =
        tvShowDao.getTvShowWithEpisodes(collectionId)
}
