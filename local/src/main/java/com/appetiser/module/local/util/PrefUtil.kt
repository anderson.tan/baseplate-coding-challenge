package com.appetiser.module.local.util

import android.content.Context
import android.content.SharedPreferences
import java.util.Date

const val PREF_MAIN = "baseplatecodingchallenge"
const val PREF_LAST_USER_VISITED = "com.appetiser.baseplate.LAST_USER_VISITED"

fun Context.getPref(): SharedPreferences =
    getSharedPreferences(PREF_MAIN, Context.MODE_PRIVATE)

fun SharedPreferences.recordPreviousVisited() =
    edit().putLong(PREF_LAST_USER_VISITED, System.currentTimeMillis()).commit()

fun SharedPreferences.setLastUserVisitedTime() =
    edit().putLong(PREF_LAST_USER_VISITED, System.currentTimeMillis()).apply()

fun SharedPreferences.getLastUserVisitedTime(): Date? =
    if (contains(PREF_LAST_USER_VISITED)) Date(
        getLong(
            PREF_LAST_USER_VISITED,
            System.currentTimeMillis()
        )
    )
    else null

fun SharedPreferences.clear() =
    edit().clear().commit()
