package com.appetiser.module.local.features.songdetails

import com.appetiser.module.local.entities.SongDB
import com.appetiser.module.local.features.songdetails.dao.SongDao
import io.reactivex.Flowable
import javax.inject.Inject

class SongLocalSourceImpl @Inject constructor(
    private val songDao: SongDao
) : SongLocalSource {

    override fun observeSong(trackId: Long): Flowable<SongDB> = songDao.getObservableSong(trackId)

    override fun observeRelatedSongs(trackId: Long, artistId: Long): Flowable<List<SongDB>> =
        songDao.getRelatedSongs(trackId, artistId)

    override fun observeSongsByAlbum(collectionId: Long): Flowable<List<SongDB>> =
        songDao.getSongsByAlbum(collectionId)
}
