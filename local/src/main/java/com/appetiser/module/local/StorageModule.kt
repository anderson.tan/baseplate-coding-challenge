package com.appetiser.module.local

import com.appetiser.module.local.features.AppRoomDatabase
import com.appetiser.module.local.features.audiobookdetails.AudioBookLocalSource
import com.appetiser.module.local.features.audiobookdetails.AudioBookLocalSourceImpl
import com.appetiser.module.local.features.moviedetails.MovieLocalSource
import com.appetiser.module.local.features.moviedetails.MovieLocalSourceImpl
import com.appetiser.module.local.features.songdetails.SongLocalSource
import com.appetiser.module.local.features.songdetails.SongLocalSourceImpl
import com.appetiser.module.local.features.storelist.StoreListLocalSource
import com.appetiser.module.local.features.storelist.StoreListLocalSourceImpl
import com.appetiser.module.local.features.tvshowdetails.TvShowLocalSource
import com.appetiser.module.local.features.tvshowdetails.TvShowLocalSourceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class StorageModule {

    @Provides
    @Singleton
    fun providesStoreListLocalSource(appDatabase: AppRoomDatabase): StoreListLocalSource {
        return StoreListLocalSourceImpl(appDatabase.storeListDao())
    }

    @Provides
    @Singleton
    fun providesTvShowLocalSource(appDatabase: AppRoomDatabase): TvShowLocalSource {
        return TvShowLocalSourceImpl(appDatabase.tvShowDao())
    }

    @Provides
    @Singleton
    fun providesSongLocalSource(appDatabase: AppRoomDatabase): SongLocalSource {
        return SongLocalSourceImpl(appDatabase.songDao())
    }

    @Provides
    @Singleton
    fun providesMovieLocalSource(appDatabase: AppRoomDatabase): MovieLocalSource {
        return MovieLocalSourceImpl(appDatabase.movieDao())
    }

    @Provides
    @Singleton
    fun providesAudioBookLocalSource(appDatabase: AppRoomDatabase): AudioBookLocalSource {
        return AudioBookLocalSourceImpl(appDatabase.audioBookDao())
    }
}
