package com.appetiser.module.local.features.storelist

import androidx.paging.DataSource
import com.appetiser.module.domain.models.storelist.ListItemType
import com.appetiser.module.domain.models.storelist.responseToTrackType
import com.appetiser.module.local.entities.AudioBookDB
import com.appetiser.module.local.entities.FeatureMovieDB
import com.appetiser.module.local.entities.ItemEntryDB
import com.appetiser.module.local.entities.SongDB
import com.appetiser.module.local.entities.StoreItem
import com.appetiser.module.local.entities.TvEpisodeDB
import com.appetiser.module.local.entities.TvShowDB
import com.appetiser.module.local.features.storelist.dao.StoreListDao
import com.appetiser.module.local.features.storelist.models.ItemDetailsDB
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class StoreListLocalSourceImpl @Inject constructor(
    private val storeListDao: StoreListDao
) : StoreListLocalSource {

    override fun insertItems(items: List<StoreItem>): Completable {
        return Completable.create {
            storeListDao.withTransaction {
                val tvEpisodes = arrayListOf<TvEpisodeDB>()
                items.forEach { item ->
                    when (item) {
                        is SongDB -> {
                            storeListDao.insertSong(item)
                        }
                        is FeatureMovieDB -> storeListDao.insertFeatureMovie(item)
                        is TvShowDB -> storeListDao.insertTvShow(item)
                        is TvEpisodeDB -> {
                            tvEpisodes.add(item)
                        }
                        is AudioBookDB -> storeListDao.insertAudioBook(item)
                    }
                    val entry = ItemEntryDB(
                        itemId = when (item.wrapperType) {
                            "track" -> when (responseToTrackType(item.kind)) {
                                ListItemType.TV_SHOW, ListItemType.AUDIOBOOK -> item.collectionId
                                else -> item.trackId
                            }
                            "audiobook" -> item.collectionId
                            else -> item.trackId
                        },
                        kind = when (item.wrapperType) {
                            "track" -> responseToTrackType(item.kind)
                            "audiobook" -> ListItemType.AUDIOBOOK
                            else -> ListItemType.UNKNOWN
                        }
                    )
                    storeListDao.insertItemEntry(entry)
                }
                storeListDao.insertTvEpisodes(tvEpisodes)
            }
            it.onComplete()
        }
    }

    override fun hasItemEntry(): Single<Boolean> = storeListDao.hasItemEntry()

    override fun clearItemEntries(): Completable = storeListDao.clearItemEntries()

    override fun getPagedItems(): Single<DataSource.Factory<Int, ItemDetailsDB>> {
        return Single.just(storeListDao.getPagedItems())
    }
}
