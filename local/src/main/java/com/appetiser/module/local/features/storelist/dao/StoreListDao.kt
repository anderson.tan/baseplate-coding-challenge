package com.appetiser.module.local.features.storelist.dao

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.appetiser.module.local.base.BaseDao
import com.appetiser.module.local.entities.AudioBookDB
import com.appetiser.module.local.entities.FeatureMovieDB
import com.appetiser.module.local.entities.ItemEntryDB
import com.appetiser.module.local.entities.SongDB
import com.appetiser.module.local.entities.TvEpisodeDB
import com.appetiser.module.local.entities.TvShowDB
import com.appetiser.module.local.features.storelist.models.ItemDetailsDB
import io.reactivex.Completable
import io.reactivex.Single

@Dao
abstract class StoreListDao : BaseDao<ItemEntryDB>() {

    @Transaction
    @Query("SELECT * FROM item_entries")
    abstract fun getPagedItems(): DataSource.Factory<Int, ItemDetailsDB>

    @Query("DELETE FROM item_entries")
    abstract fun clearItemEntries(): Completable

    @Query("SELECT EXISTS (SELECT 1 FROM item_entries LIMIT 1)")
    abstract fun hasItemEntry(): Single<Boolean>

    @Insert(entity = ItemEntryDB::class, onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertItemEntry(entry: ItemEntryDB)

    @Insert(entity = SongDB::class, onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertSong(song: SongDB): Long

    @Insert(entity = FeatureMovieDB::class, onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertFeatureMovie(featureMovie: FeatureMovieDB): Long

    @Insert(entity = TvEpisodeDB::class, onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertTvEpisodes(tvEpisodes: List<TvEpisodeDB>)

    @Insert(entity = TvEpisodeDB::class, onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertTvEpisode(tvEpisodes: TvEpisodeDB)

    @Insert(entity = TvShowDB::class, onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertTvShow(show: TvShowDB): Long

    @Insert(entity = AudioBookDB::class, onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAudioBook(audioBook: AudioBookDB): Long
}
