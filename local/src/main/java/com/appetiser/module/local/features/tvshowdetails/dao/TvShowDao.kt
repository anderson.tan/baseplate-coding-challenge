package com.appetiser.module.local.features.tvshowdetails.dao

import androidx.room.Dao
import androidx.room.Query
import com.appetiser.module.local.base.BaseDao
import com.appetiser.module.local.entities.TvShowDB
import com.appetiser.module.local.features.tvshowdetails.models.TvShowWithEpisodesDB
import io.reactivex.Flowable

@Dao
abstract class TvShowDao : BaseDao<TvShowDB>() {

    @Query("SELECT * FROM tv_shows WHERE collection_id = :collectionId")
    abstract fun getTvShowWithEpisodes(collectionId: Long): Flowable<TvShowWithEpisodesDB>
}
