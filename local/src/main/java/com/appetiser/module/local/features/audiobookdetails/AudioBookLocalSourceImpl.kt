package com.appetiser.module.local.features.audiobookdetails

import com.appetiser.module.local.entities.AudioBookDB
import com.appetiser.module.local.features.audiobookdetails.dao.AudioBookDao
import io.reactivex.Flowable
import javax.inject.Inject

class AudioBookLocalSourceImpl @Inject constructor(
    private val audioBookDao: AudioBookDao
) : AudioBookLocalSource {

    override fun observeAudioBook(collectionId: Long): Flowable<AudioBookDB> =
        audioBookDao.getAudioBook(collectionId)
}
