package com.appetiser.module.local.features.songdetails.dao

import androidx.room.Dao
import androidx.room.Query
import com.appetiser.module.local.base.BaseDao
import com.appetiser.module.local.entities.SongDB
import io.reactivex.Flowable

@Dao
abstract class SongDao : BaseDao<SongDB>() {

    @Query("SELECT * FROM songs WHERE track_id = :trackId")
    abstract fun getObservableSong(trackId: Long): Flowable<SongDB>

    @Query("SELECT * FROM songs WHERE collection_id = :collectionId ORDER BY track_number")
    abstract fun getSongsByAlbum(collectionId: Long): Flowable<List<SongDB>>

    @Query("SELECT * FROM songs WHERE artist_id = :artistId AND track_id != :currentTrackId")
    abstract fun getRelatedSongs(currentTrackId: Long, artistId: Long): Flowable<List<SongDB>>
}
