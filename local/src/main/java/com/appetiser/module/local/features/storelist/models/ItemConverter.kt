package com.appetiser.module.local.features.storelist.models

import androidx.room.TypeConverter
import com.appetiser.module.domain.models.storelist.ListItemType
import com.appetiser.module.domain.models.storelist.toTrackType

class ItemConverter {

    @TypeConverter
    fun convertToTrackType(value: String): ListItemType? {
        return toTrackType(value)
    }

    @TypeConverter
    fun convertFromTrackType(value: ListItemType): String {
        return value.type
    }
}
