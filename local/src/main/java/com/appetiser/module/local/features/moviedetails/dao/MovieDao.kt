package com.appetiser.module.local.features.moviedetails.dao

import androidx.room.Dao
import androidx.room.Query
import com.appetiser.module.local.base.BaseDao
import com.appetiser.module.local.entities.FeatureMovieDB
import io.reactivex.Flowable

@Dao
abstract class MovieDao : BaseDao<FeatureMovieDB>() {

    @Query("SELECT * FROM feature_movies WHERE track_id = :trackId")
    abstract fun getMovie(trackId: Long): Flowable<FeatureMovieDB>

    @Query(
        "SELECT * FROM feature_movies " +
                "WHERE collection_artist_id = :collectionArtistsId " +
                "ORDER BY track_number"
    )
    abstract fun getRelatedMovies(collectionArtistsId: Long): Flowable<List<FeatureMovieDB>>

    @Query(
        "SELECT * FROM feature_movies " +
                "WHERE collection_artist_id != :collectionArtistsId " +
                "AND track_id != :notIncludedTrackId"
    )
    abstract fun getUserMustLikeMovies(
        notIncludedTrackId: Long,
        collectionArtistsId: Long
    ): Flowable<List<FeatureMovieDB>>
}
