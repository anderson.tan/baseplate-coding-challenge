package com.appetiser.module.local.features.audiobookdetails

import com.appetiser.module.local.entities.AudioBookDB
import io.reactivex.Flowable

interface AudioBookLocalSource {
    fun observeAudioBook(collectionId: Long): Flowable<AudioBookDB>
}
