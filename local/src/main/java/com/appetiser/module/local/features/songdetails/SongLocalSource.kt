package com.appetiser.module.local.features.songdetails

import com.appetiser.module.local.entities.SongDB
import io.reactivex.Flowable

interface SongLocalSource {
    fun observeSong(trackId: Long): Flowable<SongDB>
    fun observeRelatedSongs(trackId: Long, artistId: Long): Flowable<List<SongDB>>
    fun observeSongsByAlbum(collectionId: Long): Flowable<List<SongDB>>
}
