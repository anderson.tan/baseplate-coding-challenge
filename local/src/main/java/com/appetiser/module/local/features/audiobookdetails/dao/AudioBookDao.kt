package com.appetiser.module.local.features.audiobookdetails.dao

import androidx.room.Dao
import androidx.room.Query
import com.appetiser.module.local.base.BaseDao
import com.appetiser.module.local.entities.AudioBookDB
import io.reactivex.Flowable

@Dao
abstract class AudioBookDao : BaseDao<AudioBookDB>() {

    @Query("SELECT * FROM audio_books WHERE collection_id = :collectionId")
    abstract fun getAudioBook(collectionId: Long): Flowable<AudioBookDB>
}
