package com.appetiser.module.local.features

import com.appetiser.module.local.features.audiobookdetails.dao.AudioBookDao
import com.appetiser.module.local.features.moviedetails.dao.MovieDao
import com.appetiser.module.local.features.songdetails.dao.SongDao
import com.appetiser.module.local.features.storelist.dao.StoreListDao
import com.appetiser.module.local.features.tvshowdetails.dao.TvShowDao

interface AppDatabase {
    fun storeListDao(): StoreListDao
    fun tvShowDao(): TvShowDao
    fun songDao(): SongDao
    fun movieDao(): MovieDao
    fun audioBookDao(): AudioBookDao
}
