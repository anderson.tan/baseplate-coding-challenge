package com.appetiser.module.local.base

import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update
import androidx.room.Delete
import androidx.room.Transaction
import com.appetiser.module.local.entities.BaseplateEntity

abstract class BaseDao<E : BaseplateEntity> {
    @Insert
    abstract fun insert(entity: E): Long

    @Insert
    abstract fun insertAll(vararg entity: E)

    @Insert
    abstract fun insertAll(entities: List<E>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAllWithReplace(entities: List<E>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAllWithReplace(vararg entity: E)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertWithReplace(entities: E): Long

    @Update
    abstract fun update(entity: E)

    @Delete
    abstract fun deleteEntity(entity: E): Int

    @Transaction
    open fun withTransaction(tx: () -> Unit) = tx()

    fun insertOrUpdate(entity: E): Long {
        return if (entity.id == 0L) {
            insert(entity)
        } else {
            update(entity)
            entity.id
        }
    }

    @Transaction
    open fun insertOrUpdate(entities: List<E>) {
        entities.forEach {
            insertOrUpdate(it)
        }
    }
}
