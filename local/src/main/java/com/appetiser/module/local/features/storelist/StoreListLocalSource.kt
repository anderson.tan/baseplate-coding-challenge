package com.appetiser.module.local.features.storelist

import androidx.paging.DataSource
import com.appetiser.module.local.entities.StoreItem
import com.appetiser.module.local.features.storelist.models.ItemDetailsDB
import io.reactivex.Completable
import io.reactivex.Single

interface StoreListLocalSource {
    fun insertItems(items: List<StoreItem>): Completable
    fun hasItemEntry(): Single<Boolean>
    fun clearItemEntries(): Completable
    fun getPagedItems(): Single<DataSource.Factory<Int, ItemDetailsDB>>
}
