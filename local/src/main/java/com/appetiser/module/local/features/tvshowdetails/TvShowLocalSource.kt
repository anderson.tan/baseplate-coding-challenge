package com.appetiser.module.local.features.tvshowdetails

import com.appetiser.module.local.features.tvshowdetails.models.TvShowWithEpisodesDB
import io.reactivex.Flowable

interface TvShowLocalSource {
    fun observeTvShowAndEpisodes(collectionId: Long): Flowable<TvShowWithEpisodesDB>
}
