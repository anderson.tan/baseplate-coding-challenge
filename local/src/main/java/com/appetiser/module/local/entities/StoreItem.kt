package com.appetiser.module.local.entities

interface StoreItem {
    val wrapperType: String
    val kind: String
    val collectionId: Long
    val trackId: Long

    companion object {
        fun empty() = object : StoreItem {
            override val wrapperType: String
                get() = ""
            override val kind: String
                get() = ""
            override val collectionId: Long
                get() = 0
            override val trackId: Long
                get() = 0
        }
    }
}
