package com.appetiser.module.local.features.moviedetails

import com.appetiser.module.local.entities.FeatureMovieDB
import io.reactivex.Flowable

interface MovieLocalSource {

    fun observeMovie(trackId: Long): Flowable<FeatureMovieDB>

    fun observeRelatedMovies(
        collectionArtistId: Long
    ): Flowable<List<FeatureMovieDB>>

    fun observeUserMustLikeMovies(
        trackId: Long,
        collectionArtistId: Long
    ): Flowable<List<FeatureMovieDB>>
}
