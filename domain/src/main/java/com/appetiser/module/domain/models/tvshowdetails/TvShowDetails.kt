package com.appetiser.module.domain.models.tvshowdetails

data class TvShowDetails(
    val id: Long = 0,
    val artworkUrl100: String = "",
    val collectionName: String = "",
    val collectionPrice: Double = 0.0,
    val contentAdvisoryRating: String = "",
    val currency: String = "",
    val longDescription: String = "",
    val primaryGenreName: String = "",
    var episodes: List<TvEpisode> = emptyList()
)