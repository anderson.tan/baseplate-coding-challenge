package com.appetiser.module.domain.models.storelist

data class ItemDetails(
    val id: Long,
    val trackName: String,
    val genre: String,
    val imageUrl: String,
    val trackPrice: Double,
    val currency: String,
    val kind: ListItemType
) {

    companion object {
        fun empty(): ItemDetails =
            ItemDetails(
                id = 0,
                trackName = "",
                genre = "",
                imageUrl = "",
                trackPrice = 0.0,
                currency = "",
                kind = ListItemType.UNKNOWN
            )
    }
}
