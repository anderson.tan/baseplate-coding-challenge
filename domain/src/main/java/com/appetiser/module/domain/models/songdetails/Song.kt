package com.appetiser.module.domain.models.songdetails

data class Song(
    val id: Long = 0,
    val artistId: Long = 0,
    val artistName: String = "",
    val artworkUrl100: String = "",
    val collectionId: Long = 0,
    val collectionName: String = "",
    val currency: String = "",
    val previewUrl: String = "",
    val primaryGenreName: String = "",
    val releaseDate: String = "",
    val trackExplicitness: String = "",
    val trackName: String = "",
    val trackNumber: Int = 0,
    val trackPrice: Double = 0.0,
    val trackTimeMillis: Long = 0
) {
    
    fun isTrackExplicit(): Boolean = trackExplicitness == "explicit"
}
