package com.appetiser.module.domain.core

data class Error(
    val message: String = "",
    val cause: Throwable? = null
)
