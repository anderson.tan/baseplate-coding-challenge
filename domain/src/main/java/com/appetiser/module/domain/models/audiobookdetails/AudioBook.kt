package com.appetiser.module.domain.models.audiobookdetails

data class AudioBook(
    val id: Long = 0,
    val artistId: Long = 0,
    val artistName: String = "",
    val artistViewUrl: String = "",
    val artworkUrl100: String = "",
    val collectionCensoredName: String = "",
    val collectionExplicitness: String = "",
    val collectionName: String = "",
    val collectionPrice: Double = 0.0,
    val collectionViewUrl: String = "",
    val copyright: String = "",
    val country: String = "",
    val currency: String = "",
    val description: String = "",
    val previewUrl: String = "",
    val primaryGenreName: String = "",
    val releaseDate: String = "",
    val trackCount: Int = 0,
    val wrapperType: String = ""
) {

    fun isExplicit(): Boolean = collectionExplicitness == "explicit"
}