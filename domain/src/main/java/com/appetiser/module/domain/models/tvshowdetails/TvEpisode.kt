package com.appetiser.module.domain.models.tvshowdetails

data class TvEpisode(
    val id: Long = 0, 
    val artworkUrl100: String = "",
    val currency: String = "",
    val longDescription: String = "",
    val trackName: String = "",
    val trackNumber: Int = 0,
    val trackPrice: Double = 0.0,
    val trackViewUrl: String = ""
)